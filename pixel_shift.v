module pixel_shift(clk, addri, addro, xshift, yshift, tile);
    input  clk;
    input  reg [15:0] addri;
    output reg [15:0] addro;
    input  reg [3:0] xshift;
    input  reg [3:0] yshift;
    reg [5:0] shiftedx;
    input  reg [7:0] tile;
    //reg [3:0] yrange;

    always @(posedge clk)
    begin
        if(addri[15:8] == tile)
        begin
            if(tile[7] == 0) //left
            begin
                if(addri[3:0] + xshift < 16)
                    addro[3:0] = addri[3:0] + xshift;
                else
                begin
                    addro[3:0] = 0;
                    addro[7:4] = 0;
                end

                /*if(addri[7:4] < 15)
                    addro[7:4] = addri[7:4] + yshift;
                else
                    addro[7:4] = 0;*/
            end
            else //right
                if(addro[3:0] > 0)
                    addro[3:0] = addri[3:0] - xshift;
                else
                    addro[3:0] = 15;
            begin
            end
        end
        else
        begin
                addro[3:0] = addri[3:0];
                addro[7:4] = addri[7:4];
        end

        //addro[7:4] = addri[7:4];
        addro[15:8] = addri[15:8];
    end

endmodule
