module LAYER_TRANSPARENCY(dataIn, dataOut);


    input [7:0] dataIn;
    output reg [7:0] dataOut;

    always
    begin
        if(dataIn == 51)
            dataOut = 27;
        else
            dataOut = dataIn;
    end

endmodule
