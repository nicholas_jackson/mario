#define __STACK			0x20
#define __STACK_FIXED
#define __HEAP			0x200
#define __HEAP_FIXED
#define __XHEAP			__HEAP
#define __XPAGE			0x00
#define __MEMORY
#define __SW_ARCH		c51
#define __SW_LSL		"51.lsl"
