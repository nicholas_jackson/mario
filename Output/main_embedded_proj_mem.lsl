#include "main_embedded_proj.lsl"
#ifdef __SW_LSL
#include __SW_LSL
#endif

derivative system
{
    core sw
    {
        architecture = __SW_ARCH;
    }

    memory internalRAM
    {
        mau = 8;
        type = ram;
        size = 256;
        map(dest=bus:sw:idata_bus, src_offset=0x0, dest_offset=0x0, size=256);
    }

    memory xram
    {
        mau = 8;
        type = ram;
        size = 32k;
        map(dest=bus:sw:xdata_bus, src_offset=0x0, dest_offset=0x0, size=32k);
    }

    memory xrom
    {
        mau = 8;
        type = rom;
        size = 8k;
        map(dest=bus:sw:code_bus, src_offset=0x0, dest_offset=0x0, size=8k);
    }



    // Software Platform locate rules
    #ifdef __SWPLATFORM__
    #include "framecfg\swplatform.lsl"
    #endif



}
