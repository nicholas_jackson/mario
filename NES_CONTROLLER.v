module nes_stub(clk, latch, pulse, data, controller_data, rst, state, iclk);

    input clk, iclk, data, rst;
    output reg latch, pulse;
    output reg [7:0] state;
    reg [7:0] s_data;
    output reg [7:0] controller_data;
    reg [7:0] count;
    reg [7:0] dcount;

    always
    begin
         if(state > 0)
            pulse = iclk;
    end

    always @(negedge iclk)
    begin
        case(state)
        1:
            s_data = ~data;
        2:
        begin
            if(dcount < 7)
            begin
                s_data = s_data << 1;
                s_data = s_data + ~data;
                dcount = dcount + 1;
            end
            else
                dcount = 0;
        end
        endcase
    end

    always @(clk)
    begin

        if(!rst)
            state = 0;

        case(state)
        0: //latch
        begin
            if(count < 2)
            begin
                count = count + 1;
                latch = 1;
            end
            else
            begin
                latch = 0;
                count = 0;
                state = state + 1;
            end
        end
        1: //delay
        begin
            state = state + 1;
        end
        2: //read data
        begin
            if(count < 14)
                count = count + 1;
            else
                count = 0;
        end
        3: //wait
        begin
            if(count < 1)
                count = count + 1;
            else
                count = 0;
        end


        endcase
    end

endmodule
