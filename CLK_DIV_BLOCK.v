module clkdiv(divisor, clk, oclk);

    input [31:0] divisor;
    input clk;
    reg [31:0] count;
    output reg oclk;

    /*initial begin
        count = 0;
        oclk = 0;
    end*/

    always @(posedge clk)
    begin
            if(count >= divisor)
            begin
                count = 0;
                oclk = ~oclk;
            end
            else
                count = count + 1;
    end

endmodule
