#include <iostream>
#include <fstream>

using namespace std;

int main()
{
    typedef enum
    {
        BACKGROUND  = 0x00,
        SHRUB_1     = 0x01,
        SHRUB_2     = 0x02,
        SHRUB_3     = 0x03,
        HILL_1      = 0x04,
        HILL_2      = 0x05,
        HILL_3      = 0x06,
        HILL_4      = 0x07,
        CASTLE_1    = 0x08,
        CASTLE_2    = 0x09,
        CASTLE_3    = 0x0A,
        CASTLE_4    = 0x0B,
        CASTLE_5    = 0x0C,
        CASTLE_6    = 0x0D,
        CASTLE_7    = 0x0E,
        CASTLE_8    = 0x0F,
        CLOUD_1     = 0x10,
        CLOUD_2     = 0x11,
        CLOUD_3     = 0x12,
        CLOUD_4     = 0x13,
        CLOUD_5     = 0x14,
        CLOUD_6     = 0x15,
        FLAG_1      = 0x16,
        FLAG_2      = 0x17,
        FLAG_3      = 0x18,
        FLAG_4      = 0x19,
        MARIO_1     = 0x1A,
        MARIO_2     = 0x1B,
        MARIO_3     = 0x1C,
        MARIO_4     = 0x1D,
        MARIO_5     = 0x1E,
        MARIO_6     = 0x1F,
        MARIO_7     = 0x20,
        COIN        = 0x21,
        BRICK1      = 0X22,
        BRICK2      = 0x23,
        PIPE_1      = 0x24,
        PIPE_2      = 0x25,
        PIPE_3      = 0x26,
        PIPE_4      = 0x27,
        FLAG_5      = 0x28,
        COINBLOCK_1 = 0x29,
        COINBLOCK_2 = 0x2A,
        MUSHROOM    = 0x2B,
        TURTLE_1    = 0x2C,
        TURTLE_2    = 0x2D,
        TURTLE_3    = 0x2E,
        TURTLE_4    = 0x2F,
        TEXT_0      = 0x30,
        TEXT_1      = 0x31,
        TEXT_2      = 0x32,
        TEXT_3      = 0x33,
        TEXT_4      = 0x34,
        TEXT_5      = 0x35,
        TEXT_6      = 0x36,
        TEXT_7      = 0x37,
        TEXT_8      = 0x38,
        TEXT_9      = 0x39,
        TEXT_M      = 0x3A,
        TEXT_A      = 0x3B,
        TEXT_R      = 0x3C,
        TEXT_I      = 0x3D,
        TEXT_O      = 0x3E
    } SPRITES_T;

    unsigned char level_1[723];
        //cloud
        level_1[0]  = 8;         //x
        level_1[1]  = 19;        //y
        level_1[2]  = CLOUD_1;   //tile
        
        level_1[3]  = 8;         //x
        level_1[4]  = 20;        //y
        level_1[5]  = CLOUD_4;   //tile
        
        level_1[6]  = 9;         //x
        level_1[7]  = 19;        //y
        level_1[8]  = CLOUD_2;   //tile
        
        level_1[9] = 9;         //x
        level_1[10] = 20;       //y
        level_1[11] = CLOUD_5;  //tile
        
        level_1[12] = 10;       //x
        level_1[13] = 19;       //y
        level_1[14] = CLOUD_3;   //tile
        
        level_1[15] = 10;       //x
        level_1[16] = 20;       //y
        level_1[17] = CLOUD_6;  //tile
        
        //shrub
        level_1[18] = 11;       //x
        level_1[19] = 28;       //y
        level_1[20] = SHRUB_1;  //tile
        
        level_1[21] = 12;
        level_1[22] = 28;
        level_1[23] = SHRUB_2;
        
        level_1[24] = 13;
        level_1[25] = 28;
        level_1[26] = SHRUB_2;
        
        level_1[27] = 14;
        level_1[28] = 28;
        level_1[29] = SHRUB_2;
        
        level_1[30] = 15;
        level_1[31] = 28;
        level_1[32] = SHRUB_3;
        
        //coin
        level_1[33] = 16;
        level_1[34] = 25;
        level_1[35] = COINBLOCK_1;
        
        //hill
        level_1[36] = 16;
        level_1[37] = 28;
        level_1[38] = HILL_2;
        
        level_1[39] = 17;
        level_1[40] = 27;
        level_1[41] = HILL_1;
        
        level_1[42] = 17;
        level_1[43] = 28;
        level_1[44] = HILL_3;
        
        level_1[45] = 18;
        level_1[46] = 28;
        level_1[47] = HILL_4;
        
        //brick landing
        level_1[48] = 20;
        level_1[49] = 25;
        level_1[50] = BRICK2;
        
        level_1[51] = 21;
        level_1[52] = 25;
        level_1[53] = BRICK2;
        
        level_1[54] = 22;
        level_1[55] = 25;
        level_1[56] = BRICK2;
        
        level_1[57] = 22;
        level_1[58] = 28;
        level_1[59] = MUSHROOM;
        
        level_1[60] = 23;
        level_1[61] = 25;
        level_1[62] = COINBLOCK_1;
        
        level_1[63] = 24;
        level_1[64] = 25;
        level_1[65] = BRICK2;
        
        // pipe 1
        level_1[66] = 28;
        level_1[67] = 27;
        level_1[68] = PIPE_1;
        
        level_1[69] = 28;
        level_1[70] = 28;
        level_1[71] = PIPE_3;
        
        level_1[72] = 29;
        level_1[73] = 27;
        level_1[74] = PIPE_2;
        
        level_1[75] = 29;
        level_1[76] = 28;
        level_1[77] = PIPE_4;
        
        //cloud 2
        level_1[78] = 31;
        level_1[79] = 19;
        level_1[80] = CLOUD_1;
        
        level_1[81] = 31;
        level_1[82] = 20;
        level_1[83] = CLOUD_4;
        
        level_1[84] = 32;
        level_1[85] = 19;
        level_1[86] = CLOUD_2;
        
        level_1[87] = 32;
        level_1[88] = 20;
        level_1[89] = CLOUD_5;
        
        level_1[90] = 33;
        level_1[91] = 19;
        level_1[92] = CLOUD_2;
        
        level_1[93] = 33;
        level_1[94] = 20;
        level_1[95] = CLOUD_5;
        
        level_1[96] = 34;
        level_1[97] = 19;
        level_1[98] = CLOUD_3;
        
        level_1[99] = 34;
        level_1[100] = 20;
        level_1[101] = CLOUD_6;
        
        //pipe
        level_1[102] = 38;
        level_1[103] = 26;
        level_1[104] = PIPE_1;
        
        level_1[105] = 38;
        level_1[106] = 27;
        level_1[107] = PIPE_3;
        
        level_1[108] = 38;
        level_1[109] = 28;
        level_1[110] = PIPE_3;
        
        level_1[111] = 39;
        level_1[112] = 26;
        level_1[113] = PIPE_2;
        
        level_1[114] = 39;
        level_1[115] = 27;
        level_1[116] = PIPE_4;
        
        level_1[117] = 39;
        level_1[118] = 28;
        level_1[119] = PIPE_4;
        
        //enemy
        level_1[120] = 41;
        level_1[121] = 28;
        level_1[122] = MUSHROOM;
        
        //pipe
        level_1[123] = 45;
        level_1[124] = 27;
        level_1[125] = PIPE_1;
        
        level_1[126] = 45;
        level_1[127] = 28;
        level_1[128] = PIPE_3;
        
        level_1[129] = 46;
        level_1[130] = 27;
        level_1[131] = PIPE_2;
        
        level_1[132] = 46;
        level_1[133] = 28;
        level_1[134] = PIPE_4;
        
        //drop
        level_1[135] = 51;
        level_1[136] = 29;
        level_1[137] = BACKGROUND;
        
        level_1[138] = 51;
        level_1[139] = 30;
        level_1[140] = BACKGROUND;
        
        level_1[141] = 51;
        level_1[142] = 31;
        level_1[143] = BACKGROUND;
        
        level_1[144] = 52;
        level_1[145] = 29;
        level_1[146] = BACKGROUND;
        
        level_1[147] = 52;
        level_1[148] = 30;
        level_1[149] = BACKGROUND;
        
        level_1[150] = 52;
        level_1[151] = 31;
        level_1[152] = BACKGROUND;
        
        //shrub
        level_1[153] = 55;
        level_1[154] = 28;
        level_1[155] = SHRUB_1;
        
        level_1[156] = 56;
        level_1[157] = 28;
        level_1[158] = SHRUB_2;
        
        level_1[159] = 57;
        level_1[160] = 28;
        level_1[161] = SHRUB_2;
        
        level_1[162] = 58;
        level_1[163] = 28;
        level_1[164] = SHRUB_2;
        
        level_1[165] = 59;
        level_1[166] = 28;
        level_1[167] = SHRUB_3;
        
        //mushroom
        
        level_1[168] = 64;
        level_1[169] = 28;
        level_1[170] = MUSHROOM;
        
        //coin
        level_1[171] = 68;
        level_1[172] = 25;
        level_1[173] = COINBLOCK_1;
        
        // turtle
        
        level_1[174] = 69;
        level_1[175] = 27;
        level_1[176] = TURTLE_1;
        
        level_1[177] = 69;
        level_1[178] = 28;
        level_1[179] = TURTLE_2;
        
        // coin
        
        level_1[180] = 72;
        level_1[181] = 25;
        level_1[182] = COINBLOCK_1;
        
        level_1[183] = 75;
        level_1[184] = 25;
        level_1[185] = COINBLOCK_1;
        
        //block ramp 1
        
        level_1[186] = 85;
        level_1[187] = 28;
        level_1[188] = FLAG_5;
        
        level_1[189] = 86;
        level_1[190] = 27;
        level_1[191] = FLAG_5;
        
        level_1[192] = 86;
        level_1[193] = 28;
        level_1[194] = FLAG_5;
        
        level_1[195] = 87;
        level_1[196] = 26;
        level_1[197] = FLAG_5;
        
        level_1[198] = 87;
        level_1[199] = 27;
        level_1[200] = FLAG_5;
        
        level_1[201] = 87;
        level_1[202] = 28;
        level_1[203] = FLAG_5;
        
        level_1[204] = 88;
        level_1[205] = 25;
        level_1[206] = FLAG_5;
        
        level_1[207] = 88;
        level_1[208] = 26;
        level_1[209] = FLAG_5;
        
        level_1[210] = 88;
        level_1[211] = 27;
        level_1[212] = FLAG_5;
        
        level_1[213] = 88;
        level_1[214] = 28;
        level_1[215] = FLAG_5;
        
        //shrub between block;
        level_1[216] = 89;
        level_1[217] = 28;
        level_1[218] = SHRUB_2;
        
        level_1[219] = 90;
        level_1[220] = 28;
        level_1[221] = SHRUB_2;
        
        //block ramp 2
        level_1[222] = 91;
        level_1[223] = 25;
        level_1[224] = FLAG_5;
        
        level_1[225] = 91;
        level_1[226] = 26;
        level_1[227] = FLAG_5;
        
        level_1[228] = 91;
        level_1[229] = 27;
        level_1[230] = FLAG_5;
        
        level_1[231] = 91;
        level_1[232] = 28;
        level_1[233] = FLAG_5;
        
        level_1[234] = 92;
        level_1[235] = 26;
        level_1[236] = FLAG_5;
        
        level_1[237] = 92;
        level_1[238] = 27;
        level_1[239] = FLAG_5;
        
        level_1[240] = 92;
        level_1[241] = 28;
        level_1[242] = FLAG_5;
        
        level_1[243] = 93;
        level_1[244] = 27;
        level_1[245] = FLAG_5;
        
        level_1[246] = 93;
        level_1[247] = 28;
        level_1[248] = FLAG_5;
        
        level_1[249] = 94;
        level_1[250] = 28;
        level_1[251] = FLAG_5;
        
        //hill
        
        level_1[252] = 95;
        level_1[253] = 28;
        level_1[254] = HILL_2;
        
        level_1[255] = 96;
        level_1[256] = 27;
        level_1[257] = HILL_2;
        
        level_1[258] = 96;
        level_1[259] = 28;
        level_1[260] = HILL_3;
        
        level_1[261] = 97;
        level_1[262] = 26;
        level_1[263] = HILL_1;
        
        level_1[264] = 97;
        level_1[265] = 27;
        level_1[266] = HILL_3;
        
        level_1[267] = 97;
        level_1[268] = 28;
        level_1[269] = HILL_3;
        
        level_1[270] = 98;
        level_1[271] = 27;
        level_1[272] = HILL_4;
        
        level_1[273] = 98;
        level_1[274] = 28;
        level_1[275] = HILL_3;
        
        //end block ramp 1
        
        level_1[276] = 99;    //
        level_1[277] = 28;
        level_1[278] = FLAG_5;
        
        level_1[279] = 100;     //
        level_1[280] = 27;
        level_1[281] = FLAG_5;
        
        level_1[282] = 100;      //
        level_1[283] = 28;
        level_1[284] = FLAG_5;
        
        level_1[285] = 101;      //
        level_1[286] = 26;
        level_1[287] = FLAG_5;
        
        level_1[288] = 101;     //
        level_1[289] = 27;
        level_1[290] = FLAG_5;
        
        level_1[291] = 101;      //
        level_1[292] = 28;
        level_1[293] = FLAG_5;
        
        level_1[294] = 102;      //
        level_1[295] = 25;
        level_1[296] = FLAG_5;
        
        level_1[297] = 102;      //
        level_1[298] = 26;
        level_1[299] = FLAG_5;
        
        level_1[300] = 102;        //
        level_1[301] = 27;
        level_1[302] = FLAG_5;
        
        level_1[303] = 102;          //
        level_1[304] = 28;
        level_1[305] = FLAG_5;
        
        level_1[306] = 103;
        level_1[307] = 25;
        level_1[308] = FLAG_5;
        
        level_1[309] = 103;
        level_1[310] = 26;
        level_1[311] = FLAG_5;
        
        level_1[312] = 103;
        level_1[313] = 27;
        level_1[314] = FLAG_5;
        
        level_1[315] = 103;
        level_1[316] = 28;
        level_1[317] = FLAG_5;
        
        //drop
        level_1[318] = 104;
        level_1[319] = 29;
        level_1[320] = BACKGROUND;
        
        level_1[321] = 104;
        level_1[322] = 30;
        level_1[323] = BACKGROUND;
        
        level_1[324] = 104;
        level_1[325] = 31;
        level_1[326] = BACKGROUND;
        
        level_1[327] = 105;
        level_1[328] = 29;
        level_1[329] = BACKGROUND;
        
        level_1[330] = 105;
        level_1[331] = 30;
        level_1[332] = BACKGROUND;
        
        level_1[333] = 105;
        level_1[334] = 31;
        level_1[335] = BACKGROUND;
        
        
        //end block ramp 2
        level_1[336] = 106;
        level_1[337] = 25;
        level_1[338] = FLAG_5;
        
        level_1[339] = 106;
        level_1[340] = 26;
        level_1[341] = FLAG_5;
        
        level_1[342] = 106;
        level_1[343] = 27;
        level_1[344] = FLAG_5;
        
        level_1[345] = 106;
        level_1[346] = 28;
        level_1[347] = FLAG_5;
        
        level_1[348] = 107;
        level_1[349] = 25;
        level_1[350] = FLAG_5;
        
        level_1[351] = 107;
        level_1[352] = 26;
        level_1[353] = FLAG_5;
        
        level_1[354] = 107;
        level_1[355] = 27;
        level_1[356] = FLAG_5;
        
        level_1[357] = 107;
        level_1[358] = 28;
        level_1[359] = FLAG_5;
        
        level_1[360] = 108;
        level_1[361] = 26;
        level_1[362] = FLAG_5;
        
        level_1[363] = 108;
        level_1[364] = 27;
        level_1[365] = FLAG_5;
        
        level_1[366] = 108;
        level_1[367] = 28;
        level_1[368] = FLAG_5;
        
        level_1[369] = 109;
        level_1[370] = 27;
        level_1[371] = FLAG_5;
        
        level_1[372] = 109;
        level_1[373] = 28;
        level_1[374] = FLAG_5;
        
        level_1[375] = 110;
        level_1[376] = 28;
        level_1[377] = FLAG_5;
        
        //pipe & cloud
        
        level_1[378] = 115;
        level_1[379] = 14;
        level_1[380] = CLOUD_1;
        
        level_1[381] = 115;
        level_1[382] = 15;
        level_1[383] = CLOUD_4;
        
        level_1[384] = 115;
        level_1[385] = 27;
        level_1[386] = PIPE_1;
        
        level_1[387] = 115;
        level_1[388] = 28;
        level_1[389] = PIPE_3;
        
        level_1[390] = 116;
        level_1[391] = 14;
        level_1[392] = CLOUD_2;
        
        level_1[393] = 116;
        level_1[394] = 15;
        level_1[395] = CLOUD_5;
        
        level_1[396] = 116;
        level_1[397] = 27;
        level_1[398] = PIPE_2;
        
        level_1[399] = 116;
        level_1[400] = 28;
        level_1[401] = PIPE_4;
        
        level_1[402] = 117;
        level_1[403] = 14;
        level_1[404] = CLOUD_3;
        
        level_1[405] = 117;
        level_1[406] = 15;
        level_1[407] = CLOUD_6;
        
        //brick
        level_1[408] = 121;
        level_1[409] = 24;
        level_1[410] = BRICK2;
        
        level_1[411] = 122;
        level_1[412] = 24;
        level_1[413] = BRICK2;
        
        level_1[414] = 123;
        level_1[415] = 24;
        level_1[416] = COINBLOCK_1;
        
        level_1[417] = 124;
        level_1[418] = 24;
        level_1[419] = BRICK2;
        
        //cloud
        level_1[420] = 125;
        level_1[421] = 15;
        level_1[422] = CLOUD_1;
        
        level_1[423] = 125;
        level_1[424] = 16;
        level_1[425] = CLOUD_4;
        
        level_1[432] = 126;
        level_1[433] = 15;
        level_1[434] = CLOUD_2;
        
        level_1[435] = 126;
        level_1[436] = 16;
        level_1[437] = CLOUD_5;
        
        level_1[444] = 127;
        level_1[445] = 15;
        level_1[446] = CLOUD_2;
        
        level_1[447] = 127;
        level_1[448] = 16;
        level_1[449] = CLOUD_5;
        
        level_1[450] = 128;
        level_1[451] = 15;
        level_1[452] = CLOUD_3;
        
        level_1[453] = 128;
        level_1[454] = 16;
        level_1[455] = CLOUD_6;
        
        //pipe
        level_1[456] = 133;
        level_1[457] = 27;
        level_1[458] = PIPE_1;
        
        level_1[459] = 133;
        level_1[460] = 28;
        level_1[461] = PIPE_3;
        
        level_1[462] = 134;
        level_1[463] = 27;
        level_1[464] = PIPE_2;
        
        level_1[465] = 134;
        level_1[466] = 28;
        level_1[467] = PIPE_4;
        
        //end ramp
        
        level_1[468] = 135;
        level_1[469] = 28;
        level_1[470] = FLAG_5;
        
        level_1[471] = 136;
        level_1[472] = 27;
        level_1[473] = FLAG_5;
        
        level_1[474] = 136;
        level_1[475] = 28;
        level_1[476] = FLAG_5;
        
        level_1[477] = 137;
        level_1[478] = 26;
        level_1[479] = FLAG_5;
        
        level_1[480] = 137;
        level_1[481] = 27;
        level_1[482] = FLAG_5;
        
        level_1[483] = 137;
        level_1[484] = 28;
        level_1[485] = FLAG_5;
        
        level_1[486] = 138;
        level_1[487] = 25;
        level_1[488] = FLAG_5;
        
        level_1[489] = 138;
        level_1[490] = 26;
        level_1[491] = FLAG_5;
        
        level_1[492] = 138;
        level_1[493] = 27;
        level_1[494] = FLAG_5;
        
        level_1[495] = 138;
        level_1[496] = 28;
        level_1[497] = FLAG_5;
        
        level_1[498] = 139;
        level_1[499] = 24;
        level_1[500] = FLAG_5;
        
        level_1[501] = 139;
        level_1[502] = 25;
        level_1[503] = FLAG_5;
        
        level_1[504] = 139;
        level_1[505] = 26;
        level_1[506] = FLAG_5;
        
        level_1[507] = 139;
        level_1[508] = 27;
        level_1[509] = FLAG_5;
        
        level_1[510] = 139;
        level_1[511] = 28;
        level_1[512] = FLAG_5;
        
        level_1[513] = 140;
        level_1[514] = 23;
        level_1[515] = FLAG_5;
        
        level_1[516] = 140;
        level_1[517] = 24;
        level_1[518] = FLAG_5;
        
        level_1[519] = 140;
        level_1[520] = 25;
        level_1[521] = FLAG_5;
        
        level_1[522] = 140;
        level_1[523] = 26;
        level_1[524] = FLAG_5;
        
        level_1[525] = 140;
        level_1[526] = 27;
        level_1[527] = FLAG_5;
        
        level_1[528] = 140;
        level_1[529] = 28;
        level_1[530] = FLAG_5;
        
        level_1[531] = 141;
        level_1[532] = 22;
        level_1[533] = FLAG_5;
        
        level_1[534] = 141;
        level_1[535] = 23;
        level_1[536] = FLAG_5;
        
        level_1[537] = 141;
        level_1[538] = 24;
        level_1[539] = FLAG_5;
        
        level_1[540] = 141;
        level_1[541] = 25;
        level_1[542] = FLAG_5;
        
        level_1[543] = 141;
        level_1[544] = 26;
        level_1[545] = FLAG_5;
        
        level_1[546] = 141;
        level_1[547] = 27;
        level_1[548] = FLAG_5;
        
        level_1[549] = 141;
        level_1[550] = 28;
        level_1[551] = FLAG_5;
        
        level_1[552] = 142;
        level_1[553] = 21;
        level_1[554] = FLAG_5;
        
        level_1[555] = 142;
        level_1[556] = 22;
        level_1[557] = FLAG_5;
        
        level_1[558] = 142;
        level_1[559] = 23;
        level_1[560] = FLAG_5;
        
        level_1[561] = 142;
        level_1[562] = 24;
        level_1[563] = FLAG_5;
        
        level_1[564] = 142;
        level_1[565] = 25;
        level_1[566] = FLAG_5;
        
        level_1[567] = 142;
        level_1[568] = 26;
        level_1[569] = FLAG_5;
        
        level_1[570] = 142;
        level_1[571] = 27;
        level_1[572] = FLAG_5;
        
        level_1[573] = 142;
        level_1[574] = 28;
        level_1[575] = FLAG_5;
        
        level_1[576] = 143;
        level_1[577] = 21;
        level_1[578] = FLAG_5;
        
        level_1[579] = 143;
        level_1[580] = 22;
        level_1[581] = FLAG_5;
        
        level_1[582] = 143;
        level_1[583] = 23;
        level_1[584] = FLAG_5;
        
        level_1[585] = 143;
        level_1[586] = 24;
        level_1[587] = FLAG_5;
        
        level_1[588] = 143;
        level_1[589] = 25;
        level_1[590] = FLAG_5;
        
        level_1[591] = 143;
        level_1[592] = 26;
        level_1[593] = FLAG_5;
        
        level_1[594] = 143;
        level_1[595] = 27;
        level_1[596] = FLAG_5;
        
        level_1[597] = 143;
        level_1[598] = 28;
        level_1[599] = FLAG_5;
        
        //flag pole
        
        level_1[600] = 150;
        level_1[601] = 20;
        level_1[602] = FLAG_2;
        
        level_1[603] = 151;
        level_1[604] = 19;
        level_1[605] = FLAG_1;
        
        level_1[606] = 151;
        level_1[607] = 20;
        level_1[608] = FLAG_3;
        
        level_1[609] = 151;
        level_1[610] = 21;
        level_1[611] = FLAG_4;
        
        level_1[612] = 151;
        level_1[613] = 22;
        level_1[614] = FLAG_4;
        
        level_1[615] = 151;
        level_1[616] = 23;
        level_1[617] = FLAG_4;
        
        level_1[618] = 151;
        level_1[619] = 24;
        level_1[620] = FLAG_4;
        
        level_1[621] = 151;
        level_1[622] = 25;
        level_1[623] = FLAG_4;
        
        level_1[624] = 151;
        level_1[625] = 26;
        level_1[626] = FLAG_4;
        
        level_1[627] = 151;
        level_1[628] = 27;
        level_1[629] = FLAG_4;
        
        level_1[630] = 151;
        level_1[631] = 28;
        level_1[632] = FLAG_5;
        
        //cloud
        level_1[633] = 153;
        level_1[634] = 19;
        level_1[635] = CLOUD_1;
        
        level_1[636] = 153;
        level_1[637] = 20;
        level_1[638] = CLOUD_4;
        
        level_1[639] = 154;
        level_1[640] = 19;
        level_1[641] = CLOUD_3;
        
        level_1[642] = 154;
        level_1[643] = 20;
        level_1[644] = CLOUD_6;
        
        //castle
        level_1[645] = 155;
        level_1[646] = 26;
        level_1[647] = CASTLE_2;
        
        level_1[648] = 155;
        level_1[649] = 27;
        level_1[650] = CASTLE_4;
        
        level_1[651] = 155;
        level_1[652] = 28;
        level_1[653] = CASTLE_4;
        
        level_1[654] = 156;
        level_1[655] = 24;
        level_1[656] = CASTLE_2;
        
        level_1[657] = 156;
        level_1[658] = 25;
        level_1[659] = CASTLE_3;
        
        level_1[660] = 156;
        level_1[661] = 26;
        level_1[662] = CASTLE_7;
        
        level_1[663] = 156;
        level_1[664] = 27;
        level_1[665] = CASTLE_4;
        
        level_1[666] = 156;
        level_1[667] = 28;
        level_1[668] = CASTLE_4;
        
        level_1[669] = 157;
        level_1[670] = 24;
        level_1[671] = CASTLE_2;
        
        level_1[672] = 157;
        level_1[673] = 25;
        level_1[674] = CASTLE_4;
        
        level_1[675] = 157;
        level_1[676] = 26;
        level_1[677] = CASTLE_7;
        
        level_1[678] = 157;
        level_1[679] = 27;
        level_1[680] = CASTLE_5;
        
        level_1[681] = 157;
        level_1[682] = 28;
        level_1[683] = CASTLE_6;
        
        level_1[684] = 158;
        level_1[685] = 24;
        level_1[686] = CASTLE_2;
        
        level_1[687] = 158;
        level_1[688] = 25;
        level_1[689] = CASTLE_8;
        
        level_1[690] = 158;
        level_1[691] = 26;
        level_1[692] = CASTLE_7;
        
        level_1[693] = 158;
        level_1[694] = 27;
        level_1[695] = CASTLE_4;
        
        level_1[696] = 158;
        level_1[697] = 28;
        level_1[698] = CASTLE_4;
        
        level_1[699] = 159;
        level_1[700] = 26;
        level_1[701] = CASTLE_2;
        
        level_1[702] = 159;
        level_1[703] = 27;
        level_1[704] = CASTLE_4;
        
        level_1[705] = 159;
        level_1[706] = 28;
        level_1[707] = CASTLE_4;
        
        //end shrub
        
        level_1[708] = 160;
        level_1[709] = 28;
        level_1[710] = SHRUB_3;
        
        //end hill
        level_1[711] = 161;
        level_1[712] = 28;
        level_1[713] = HILL_2;
        
        level_1[714] = 162;
        level_1[715] = 27;
        level_1[716] = HILL_1;
        
        level_1[717] = 162;
        level_1[718] = 28;
        level_1[719] = HILL_3;
        
        level_1[720] = 163;
        level_1[721] = 28;
        level_1[722] = HILL_4;

    /*cout << "unsigned char level_1[723] = {";
    
    for(size_t i = 0; i < 723; i+=3)
    {
        cout << dec << (size_t)level_1[i] << ", ";
        cout << dec << (size_t)level_1[i+1] << ", ";
        cout << hex << "0x" << (size_t)level_1[i+2] << ", ";
    }
    cout << "};";*/
    
    ofstream out("level.bin", ios::out | ios::binary);
    
    for(size_t i = 0; i < 723; i++)
    {
        out << level_1[i];
    }
    
    out.close();
    
    /*
    unsigned char level_2[723] = {8, 19, 0x10, 8, 20, 0x13, 9, 19, 0x11, 9, 20, 0x14, 10, 19, 0x12, 10, 20, 0x15, 11, 28, 0x1, 12, 28, 0x2, 13, 28, 0x2, 14, 28, 0x2, 15, 28, 0x3, 16, 25, 0x29, 16, 28, 0x5, 17, 27, 0x4, 17, 28, 0x6, 18, 28, 0x7, 20, 25, 0x23, 21, 25, 0x23, 22, 25, 0x23, 22, 28, 0x2b, 23, 25, 0x29, 24, 25, 0x23, 28, 27, 0x24, 28, 28, 0x26, 29, 27, 0x25, 29, 28, 0x27, 31, 19, 0x10, 31, 20, 0x13, 32, 19, 0x11, 32, 20, 0x14, 33, 19, 0x11, 33, 20, 0x14, 34, 19, 0x12, 34, 20, 0x15, 38, 26, 0x24, 38, 27, 0x26, 38, 28, 0x26, 39, 26, 0x25, 39, 27, 0x27, 39, 28, 0x27, 41, 28, 0x2b, 45, 27, 0x24, 45, 28, 0x26, 46, 27, 0x25, 46, 28, 0x27, 51, 29, 0x0, 51, 30, 0x0, 51, 31, 0x0, 52, 29, 0x0, 52, 30, 0x0, 52, 31, 0x0, 55, 28, 0x1, 56, 28, 0x2, 57, 28, 0x2, 58, 28, 0x2, 59, 28, 0x3, 64, 28, 0x2b, 68, 25, 0x29, 69, 27, 0x2c, 69, 28, 0x2d, 72, 25, 0x29, 75, 25, 0x29, 85, 28, 0x28, 86, 27, 0x28, 86, 28, 0x28, 87, 26, 0x28, 87, 27, 0x28, 87, 28, 0x28, 88, 25, 0x28, 88, 26, 0x28, 88, 27, 0x28, 88, 28, 0x28, 89, 28, 0x2, 90, 28, 0x2, 91, 25, 0x28, 91, 26, 0x28, 91, 27, 0x28, 91, 28, 0x28, 92, 26, 0x28, 92, 27, 0x28, 92, 28, 0x28, 93, 27, 0x28, 93, 28, 0x28, 94, 28, 0x28, 95, 28, 0x5, 96, 27, 0x5, 96, 28, 0x6, 97, 26, 0x4, 97, 27, 0x6, 97, 28, 0x6, 98, 27, 0x7, 98, 28, 0x6, 99, 28, 0x28, 100, 27, 0x28, 100, 28, 0x28, 101, 26, 0x28, 101, 27, 0x28, 101, 28, 0x28, 102, 25, 0x28, 102, 26, 0x28, 102, 27, 0x28, 102, 28, 0x28, 103, 25, 0x28, 103, 26, 0x28, 103, 27, 0x28, 103, 28, 0x28, 104, 29, 0x0, 104, 30, 0x0, 104, 31, 0x0, 105, 29, 0x0, 105, 30, 0x0, 105, 31, 0x0, 106, 25, 0x28, 106, 26, 0x28, 106, 27, 0x28, 106, 28, 0x28, 107, 25, 0x28, 107, 26, 0x28, 107, 27, 0x28, 107, 28, 0x28, 108, 26, 0x28, 108, 27, 0x28, 108, 28, 0x28, 109, 27, 0x28, 109, 28, 0x28, 110, 28, 0x28, 115, 14, 0x10, 115, 15, 0x13, 115, 27, 0x24, 115, 28, 0x26, 116, 14, 0x11, 116, 15, 0x14, 116, 27, 0x25, 116, 28, 0x27, 117, 14, 0x12, 117, 15, 0x15, 121, 24, 0x23, 122, 24, 0x23, 123, 24, 0x29, 124, 24, 0x23, 125, 15, 0x10, 125, 16, 0x13, 0, 0, 0x0, 0, 0, 0x0, 126, 15, 0x11, 126, 16, 0x14, 0, 0, 0x0, 0, 0, 0x0, 127, 15, 0x11, 127, 16, 0x14, 128, 15, 0x12, 128, 16, 0x15, 133, 27, 0x24, 133, 28, 0x26, 134, 27, 0x25, 134, 28, 0x27, 135, 28, 0x28, 136, 27, 0x28, 136, 28, 0x28, 137, 26, 0x28, 137, 27, 0x28, 137, 28, 0x28, 138, 25, 0x28, 138, 26, 0x28, 138, 27, 0x28, 138, 28, 0x28, 139, 24, 0x28, 139, 25, 0x28, 139, 26, 0x28, 139, 27, 0x28, 139, 28, 0x28, 140, 23, 0x28, 140, 24, 0x28, 140, 25, 0x28, 140, 26, 0x28, 140, 27, 0x28, 140, 28, 0x28, 141, 22, 0x28, 141, 23, 0x28, 141, 24, 0x28, 141, 25, 0x28, 141, 26, 0x28, 141, 27, 0x28, 141, 28, 0x28, 142, 21, 0x28, 142, 22, 0x28, 142, 23, 0x28, 142, 24, 0x28, 142, 25, 0x28, 142, 26, 0x28, 142, 27, 0x28, 142, 28, 0x28, 143, 21, 0x28, 143, 22, 0x28, 143, 23, 0x28, 143, 24, 0x28, 143, 25, 0x28, 143, 26, 0x28, 143, 27, 0x28, 143, 28, 0x28, 150, 20, 0x17, 151, 19, 0x16, 151, 20, 0x18, 151, 21, 0x19, 151, 22, 0x19, 151, 23, 0x19, 151, 24, 0x19, 151, 25, 0x19, 151, 26, 0x19, 151, 27, 0x19, 151, 28, 0x28, 153, 19, 0x10, 153, 20, 0x13, 154, 19, 0x12, 154, 20, 0x15, 155, 26, 0x9, 155, 27, 0xb, 155, 28, 0xb, 156, 24, 0x9, 156, 25, 0xa, 156, 26, 0xe, 156, 27, 0xb, 156, 28, 0xb, 157, 24, 0x9, 157, 25, 0xb, 157, 26, 0xe, 157, 27, 0xc, 157, 28, 0xd, 158, 24, 0x9, 158, 25, 0xf, 158, 26, 0xe, 158, 27, 0xb, 158, 28, 0xb, 159, 26, 0x9, 159, 27, 0xb, 159, 28, 0xb, 160, 28, 0x3, 161, 28, 0x5, 162, 27, 0x4, 162, 28, 0x6, 163, 28, 0x7};*/
    
    return 0;
    
}