#ifndef MARIO_LEVEL_H
#define MARIO_LEVEL_H

#include "mario.h"
#include "game.h"
#include "hardware.h"
#include "enemy.h"

#ifndef TRUE
#define TRUE 0x01
#endif
#ifndef FALSE
#define FALSE 0x00
#endif

#define MAXIMUM_SCROLL 144

//typedef unsigned char BOOL;

typedef enum
{
    BACKGROUND  = 0x00,
    SHRUB_1     = 0x01,
    SHRUB_2     = 0x02,
    SHRUB_3     = 0x03,
    HILL_1      = 0x04,
    HILL_2      = 0x05,
    HILL_3      = 0x06,
    HILL_4      = 0x07,
    CASTLE_1    = 0x08,
    CASTLE_2    = 0x09,
    CASTLE_3    = 0x0A,
    CASTLE_4    = 0x0B,
    CASTLE_5    = 0x0C,
    CASTLE_6    = 0x0D,
    CASTLE_7    = 0x0E,
    CASTLE_8    = 0x0F,
    CLOUD_1     = 0x10,
    CLOUD_2     = 0x11,
    CLOUD_3     = 0x12,
    CLOUD_4     = 0x13,
    CLOUD_5     = 0x14,
    CLOUD_6     = 0x15,
    FLAG_1      = 0x16,
    FLAG_2      = 0x17,
    FLAG_3      = 0x18,
    FLAG_4      = 0x19,
    MARIO_1     = 0x1A,
    MARIO_2     = 0x1B,
    MARIO_3     = 0x1C,
    MARIO_4     = 0x1D,
    MARIO_5     = 0x1E,
    MARIO_6     = 0x1F,
    MARIO_7     = 0x20,
    COIN        = 0x21,
    BRICK1      = 0X22,
    BRICK2      = 0x23,
    PIPE_1      = 0x24,
    PIPE_2      = 0x25,
    PIPE_3      = 0x26,
    PIPE_4      = 0x27,
    FLAG_5      = 0x28,
    COINBLOCK_1 = 0x29,
    COINBLOCK_2 = 0x2A,
    MUSHROOM    = 0x2B,
    TURTLE_1    = 0x2C,
    TURTLE_2    = 0x2D,
    TURTLE_3    = 0x2E,
    TURTLE_4    = 0x2F,
    TEXT_0      = 0x30,
    TEXT_1      = 0x31,
    TEXT_2      = 0x32,
    TEXT_3      = 0x33,
    TEXT_4      = 0x34,
    TEXT_5      = 0x35,
    TEXT_6      = 0x36,
    TEXT_7      = 0x37,
    TEXT_8      = 0x38,
    TEXT_9      = 0x39,
    TEXT_M      = 0x3A,
    TEXT_A      = 0x3B,
    TEXT_R      = 0x3C,
    TEXT_I      = 0x3D,
    TEXT_O      = 0x3E
} SPRITES_T;

typedef struct MARIO_S;

typedef struct
{
    unsigned char x;
    unsigned char y;
    unsigned char TILE;
} LAYER_S;

typedef struct
{
    unsigned char lives;
    unsigned int coins;
    unsigned char level;
    unsigned char flag;
    MARIO_S * mario;
} GAME_S;

unsigned char print(void);
void init_level(GAME_S * game);
void write_tile(unsigned char indexX, unsigned char indexY, unsigned char data);
unsigned char read_tile(unsigned char indexX, unsigned char indexY);
unsigned char read_level(unsigned short index);
void move_mario(MARIO_S * mario, unsigned char direction);
void move_tile(unsigned char x, unsigned char y, unsigned char tile, unsigned char direction);
void move_tile_with_priority(unsigned char x, unsigned char y, unsigned char tile, unsigned char direction, unsigned char priority);
void tick(MARIO_S * mario);
unsigned char check_collisions(MARIO_S * mario, unsigned char direction);
void write_score(unsigned int score);
void write_lives(unsigned char lives);
void update_screen(unsigned char left_bound, MARIO_S * mario);
void move_enemies(MARIO_S * mario);
void kill_mario(MARIO_S * mario);
void jump_mario(MARIO_S * mario);
BOOL check_enemy_collisions(ENEMY_S * enemy, unsigned char direction);
void init_enemies(void);
void animate_flag(MARIO_S * mario);
GAME_S * games(void);

#endif
