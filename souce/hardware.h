#ifndef HARDWARE_HEADER_H
#define HARDWARE_HEADER_H

#include <regtsk51a.sfr>
#include "level.h"
//#include "gam

#ifndef         IE
# define        IE  (*(__bsfr volatile unsigned char *)0xA8)
#endif

#ifndef         IP
# define        IP  (*(__bsfr volatile unsigned char *)0xB8)
#endif

#ifndef         TCON
# define        TCON  (*(__bsfr volatile unsigned char *)0x88)
#endif

#ifndef         TMOD
#define         TMOD  (*(__bsfr volatile unsigned char *)0x89)
#endif

#ifndef         TL0
#define         TL0  (*(__bsfr volatile unsigned char *)0x8A)
#endif

#ifndef         TH0
#define         TH0  (*(__bsfr volatile unsigned char *)0x8C)
#endif

#ifndef         P1_1
# define        P1_1  (*(__bsfr volatile unsigned char *)0x90))
#endif

#define KEY_DOWN        0x72
#define KEY_UP          0x75
#define KEY_LEFT        0x6B
#define KEY_RIGHT       0x74
#define SPACE           0x29
#define RELEASE         0xF0
#define WINDOW_H        32
#define WINDOW_V        32
#define NUM_OBJECTS     724

#ifndef TRUE
#define TRUE 0x01
#endif
#ifndef FALSE
#define FALSE 0x00
#endif

/*
Location Service
0003h External Interrupt 0
000Bh Timer 0 overflow
0013h External Interrupt 1
001Bh Timer 1 overflow
0023h Serial Port Interrupt
*/

typedef struct
{
    unsigned char state;
} KEYBOARD_STATE_S;

typedef unsigned char BYTE;
typedef unsigned short WORD;

void write_memory(unsigned short index, unsigned char data);
unsigned char read_memory(unsigned short index);
void init_hardware(void);
void init_interrupts(void);
void init_keyboard(void);
void delay(unsigned short cycles);
__interrupt( __INTNO(0) )void keyboard_int( void );
void play_sound(unsigned char tone);

#endif
