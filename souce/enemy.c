#include "enemy.h"
#include <stdlib.h>

/*
typedef struct
{
    COORD_S cart;
    COORD_S start_cart;
    unsigned char size;
    unsigned char TILE[2];
    unsigned char visible;
    unsigned char layer_priority;
    unsigned char distance;
    unsigned char direction;

} ENEMY_S;
*/

void move_enemy_direction(ENEMY_S * enemy)
{
    if(abs(enemy->start_x - enemy->x) == 0 && enemy->direction == STILL)
    {
        enemy->direction = LEFT;
    }
    else if(abs(enemy->start_x - enemy->x) == 3 && enemy->direction == LEFT)
    {
        enemy->direction = RIGHT;
    }
    else if(abs(enemy->start_x - enemy->x) == 3 && enemy->direction == RIGHT)
    {
        enemy->direction = LEFT;
    }
}

void set_enemy_visibility(ENEMY_S * enemy, unsigned char visibility)
{
    enemy->visible = visibility;
}

void set_layer_priority(ENEMY_S * enemy, unsigned char layer)
{
    enemy->layer_priority = layer;
}

void set_enemy_direction(ENEMY_S * enemy, unsigned char direction)
{
    enemy->direction = direction;
}
