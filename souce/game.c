#include "game.h"

unsigned char scroll;
unsigned char buttonQueue;
BUTTONQUEUE_S   bqueue;
MARIO_S         mario;
GAME_S          game;
KEYBOARD_S      kb;

void init_game(void)
{

    init_hardware();
    bqueue.size = 0;
    scroll = 0;

    game.flag = FALSE;
    game.lives = 3;
    game.coins = 0;
    kb.kb_hit = 0;
    init_level(&game);
    write_tile(3, 3, TEXT_M);
    write_tile(4, 3, TEXT_A);
    write_tile(5, 3, TEXT_R);
    write_tile(6, 3, TEXT_I);
    write_tile(7, 3, TEXT_O);
    write_score(0);

    write_tile(10, 3, MARIO_3);
    write_lives(game.lives);

    mario.coord.x = 3;
    mario.coord.y = 28;
    mario.TILE = MARIO_3;
    mario.jumping = TRUE;
    write_tile(mario.coord.x, mario.coord.y, mario.TILE);

}

void start_game(void)
{
    init_game();
    unsigned char i = 0;
    while(1)
    {
        poll_button_queue();
        tick(&mario);
        delay(0xFFFF);
    }
}

void poll_button_queue(void)
{
    if(kb.kb_hit == 0x01)
    {
            button_handler(kb.kb_data);
            kb.kb_hit = 0x0;
    }
    delay(0xF);
}

void add_button_queue(unsigned char key)
{
    kb.kb_hit = 0x1;
    kb.kb_data = key;
    if(bqueue.size < MAXQUEUE)
    {
        bqueue.queue[bqueue.size++] = key;
    }
}

void reset_game(void)
{
    init_game();
}

void detect_gap(void)
{
}

void button_handler(unsigned char key)
{
    if(key == KEY_RIGHT)
    {
        P1 = 0x1 << 0;
        move_mario(&mario, RIGHT);

    }
    else if(key == KEY_LEFT)
    {
        P1 = 0x1 << 1;
        move_mario(&mario, LEFT);
    }
    else if(key == SPACE)
    {
        P1 = 0x1 << 2;
        play_sound(1);
        //move_mario(&mario, UP);
        //move_mario(&mario, DOWN);

    }
    else if(key == KEY_DOWN)
    {
        move_mario(&mario, DOWN);
    }
    else
    {
        //not recognised
    }

    /*if(mario.TILE == MARIO_2)
        mario.TILE = MARIO_3;
    else if(mario.TILE == MARIO_3)
        mario.TILE = MARIO_2;*/
}


