#ifndef MARIO_MARIO_H
#define MARIO_MARIO_H

#define KILL_MARIO 0xFF

typedef struct
{
    unsigned char x;
    unsigned char y;
} COORD_S;

typedef struct
{
    COORD_S coord;
    unsigned char TILE;
    unsigned char jumping;
    unsigned char layer_priority;
    unsigned char direction;
} MARIO_S;

typedef unsigned char BOOL;

void update_position(MARIO_S * mario, unsigned char x, unsigned char y);
void update_priority(MARIO_S * mario, unsigned char priority);
void update_mario_jump(MARIO_S * mario, BOOL jumping);
void animate_to(void);
void update_mario_direction(MARIO_S * mario, unsigned char direction);

#endif
