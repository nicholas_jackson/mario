#include "level.h"
#include "mario.h"

__xdata unsigned char __xdata window[32][32] __at(0x0);
unsigned char __xdata level[1024] __at(0x800);
unsigned char numberActiveEnemies;
ENEMY_S         enemies[4];
unsigned char left_window_bound;
unsigned char numberOfLayers;
unsigned char activeLayer;
LAYER_S         layers[2];
GAME_S  *       p_game;

void init_level(GAME_S * game)
{
    //init_enemies();
    p_game = game;
    left_window_bound = 0;
    layers[0].x = 3;
    layers[0].y = 28;
    layers[0].TILE = BACKGROUND;
    numberActiveEnemies = 0;

    for(int i = 0; i < 32; i++)
    {
        for(int j = 0; j < 32; j++)
        {
            write_tile((BYTE)j, (BYTE)i, BACKGROUND);
        }
    }

    for(int i = 0; i< 32; i++)
    {
        write_tile((BYTE)i, 29, BRICK1);
        write_tile((BYTE)i, 30, BRICK1);
        write_tile((BYTE)i, 31, BRICK1);
    }

    unsigned char i = 0;
    while(1)
    {
        if(read_level(i) < WINDOW_H)
        {
            if(read_level(i+2) == MUSHROOM)
            {
                enemies[numberActiveEnemies].visible = 0x1;
                enemies[numberActiveEnemies].start_x = read_level(i);
                enemies[numberActiveEnemies].x = enemies[numberActiveEnemies].start_x;
                enemies[numberActiveEnemies].direction = STILL;
                enemies[numberActiveEnemies].TILE[0] = MUSHROOM;
                enemies[numberActiveEnemies].size = 1;
                enemies[numberActiveEnemies].start_y = read_level(i+1);
                enemies[numberActiveEnemies].y = enemies[numberActiveEnemies++].start_y;
            }

            write_tile(read_level(i), read_level(i+1), read_level(i+2));
        }
        else
        {
            break;
        }

        i+=3;
    }
}

void write_tile(unsigned char indexX, unsigned char indexY, unsigned char data)
{
    if(indexX < WINDOW_H && indexY < WINDOW_V)
    {
        window[indexY][indexX] = data;
    }
}

unsigned char read_tile(unsigned char indexX, unsigned char indexY)
{
    if(indexX < WINDOW_H && indexY < WINDOW_V)
    {
        return window[indexY][indexX];
    }

    return 0xFF;
}

BYTE read_level(WORD index)
{
    if(index < 1024)
    {
        return level[index];
    }

    return 0xFF;
}

void move_mario(MARIO_S * mario, unsigned char direction)
{
    unsigned char x = mario->coord.x;
    unsigned char y = mario->coord.y;

    update_mario_direction(mario, direction);

    if(direction == RIGHT)
    {
        if(check_collisions(mario, RIGHT) == TRUE)
        {
            update_mario_direction(mario, RIGHT);
            move_tile_with_priority(mario->coord.x, mario->coord.y, mario->TILE, RIGHT, 0x0);
            update_position(mario, x+1, y);
        }
    }
    else if(direction == LEFT)
    {
        if(check_collisions(mario, LEFT) == TRUE)
        {
            update_mario_direction(mario, RIGHT);
            move_tile_with_priority(mario->coord.x, mario->coord.y, mario->TILE, LEFT, 0x0);
            update_position(mario, x-1, y);
        }
    }
    else if(direction == UP)
    {
        check_collisions(mario, UP);
        jump_mario(mario);
    }
    else if(direction == DOWN)
    {
        if(check_collisions(mario, DOWN) == TRUE)
        {
            move_tile_with_priority(mario->coord.x, mario->coord.y, mario->TILE, DOWN, 0x0);
            update_position(mario, x, y+1);
        }
    }
}

void move_tile(unsigned char x, unsigned char y, unsigned char tile, unsigned char direction)
{
    switch(direction)
    {
        case LEFT:
            write_tile(x-1, y, tile);
            break;
        case RIGHT:
            write_tile(x+1, y, tile);
            break;
    }
}

void move_tile_with_priority(unsigned char x, unsigned char y, unsigned char tile, unsigned char direction, unsigned char priority)
{
    write_tile(layers[0].x, layers[0].y, layers[0].TILE);

    switch(direction)
    {
        case RIGHT:
            layers[0].x = x + 1;
            layers[0].y = y;
            layers[0].TILE = read_tile(x + 1, y);
            write_tile(x + 1, y, tile);
            break;
        case LEFT:
            layers[0].x = x - 1;
            layers[0].y = y;
            layers[0].TILE = read_tile(x - 1, y);
            write_tile(x - 1, y, tile);
            break;
            case UP:
            layers[0].x = x;
            layers[0].y = y - 1;
            layers[0].TILE = read_tile(x, y - 1);
            write_tile(x, y - 1, tile);
            break;
        case DOWN:
            layers[0].x = x;
            layers[0].y = y + 1;
            layers[0]. TILE = read_tile(x, y + 1);
            write_tile(x, y + 1, tile);
            break;
    }
}

void tick(MARIO_S * mario)
{
    move_enemies(mario);
}

void write_score(unsigned int score)
{
    double s_val = (double)score;
    unsigned char index = 0;
    unsigned char tile_score[6] = {TEXT_0, TEXT_0, TEXT_0, TEXT_0, TEXT_0, TEXT_0};
    while (s_val > 0.0 && index < 6)
    {
        tile_score[index++] = 0x30 + ((unsigned int)s_val % 10);
        s_val = ((unsigned int)s_val/10);
    }

    for(unsigned char i = 0; i < 6; i++)
    {
        write_tile((3 + i), 4, tile_score[5-i]);
    }

}

void write_lives(unsigned char lives)
{
    double l_val = (double)lives;
    unsigned char index = 0;
    unsigned char tile_lives[2] = {TEXT_0, TEXT_0};
    while(l_val > 0.0 && index < 2)
    {
        tile_lives[index++] = 0x30 + ((unsigned int)l_val % 10);
        l_val = ((unsigned int)l_val/10);
    }

    for(unsigned char i = 0; i < 2; i++)
    {
        write_tile((12 + i), 3, tile_lives[1-i]);
    }
}

void update_screen(unsigned char left_bound, MARIO_S * mario)
{
    left_window_bound += left_bound;

    if(left_window_bound < MAXIMUM_SCROLL)
    {

        for(int i = 0; i < NUMENEMIES; i++)
        {
            if(enemies[i].visible == TRUE)
                --enemies[i].x;
        }

        for(int i = 5; i < 29; i++)
        {
            for(int j = 0; j < 32; j++)
            {
                if(j == 31)
                {
                    write_tile((BYTE)j, (BYTE)i, 0);
                }
                else
                {
                    if(read_tile(j+1, i) != MARIO_1 && read_tile(j+1, i) != MARIO_2 && read_tile(j+1, i) != MARIO_3)
                        write_tile((BYTE) j, (BYTE) i, read_tile((BYTE) j+1, (BYTE) i));
                }
            }
        }

        for(int i = 0; i < NUMENEMIES; i++)
        {
            --enemies[i].start_x;
            if(enemies[i].x < 1 && enemies[i].visible == TRUE)
            {
                set_enemy_visibility(&enemies[i], FALSE);
                //--numberActiveEnemies;
            }
        }

        int i = 0;
        while(i <= NUM_OBJECTS)
        {
            if(read_level(i)  < WINDOW_H + left_window_bound)
            {
                if(read_level(i) == WINDOW_H + left_window_bound - 1)
                {
                    switch(read_level(i + 2))
                    {
                        case MUSHROOM:
                            enemies[numberActiveEnemies].visible = TRUE;
                            enemies[numberActiveEnemies].start_x = read_level(i) - left_window_bound;
                            enemies[numberActiveEnemies].x = enemies[numberActiveEnemies].start_x;
                            enemies[numberActiveEnemies].direction = STILL;
                            enemies[numberActiveEnemies].TILE[0] = MUSHROOM;
                            enemies[numberActiveEnemies].size = 1;
                            enemies[numberActiveEnemies].start_y = read_level(i+1);
                            enemies[numberActiveEnemies].y = enemies[numberActiveEnemies++].start_y;
                            break;
                        case TURTLE_2:
                            enemies[numberActiveEnemies].visible = TRUE;
                            enemies[numberActiveEnemies].start_x = read_level(i) - left_window_bound;
                            enemies[numberActiveEnemies].x = enemies[numberActiveEnemies].start_x;
                            enemies[numberActiveEnemies].direction = STILL;
                            enemies[numberActiveEnemies].TILE[0] = TURTLE_1;
                            enemies[numberActiveEnemies].TILE[1] = TURTLE_2;
                            enemies[numberActiveEnemies].size = 2;
                            enemies[numberActiveEnemies].start_y = read_level(i+1);
                            enemies[numberActiveEnemies].y = enemies[numberActiveEnemies++].start_y;
                            break;

                    }

                    write_tile((read_level(i) - left_window_bound),read_level(i+1),read_level(i+2));
                }
            }
            else
            {
                break;
            }

            i += 3;
        }
    }
}

unsigned char check_collisions(MARIO_S * mario, unsigned char direction)
{
    if(direction == RIGHT)
    {
        if(mario->coord.x+1 < WINDOW_H - 2)
        {
            if(read_tile(mario->coord.x+1, mario->coord.y) < BRICK1)
            {
                switch(read_tile(mario->coord.x+1, mario->coord.y))
                {
                    case COIN:
                        p_game->coins += 100;
                        write_score(p_game->coins);
                        write_tile(mario->coord.x+1, mario->coord.y, BACKGROUND);
                        return TRUE;
                    case FLAG_3:
                    case FLAG_4:
                        animate_flag(mario);
                        return TRUE;
                    default:
                        return TRUE;
                }
            }
            else
            {
                switch(read_tile(mario->coord.x + 1, mario->coord.y))
                {
                    case MUSHROOM:
                        kill_mario(mario);
                        break;
                    case TURTLE_1:
                    case TURTLE_2:
                    case TURTLE_3:
                    case TURTLE_4:
                        kill_mario(mario);
                        break;
                }

                return FALSE; //stop at solid objects
            }
        }
        else
        {
            update_screen(1, mario);
            //update_position(mario, mario->coord.x-1, mario->coord.y);
            return FALSE;
        }
    }
    else if(direction == LEFT)
    {
        if(mario->coord.x > 0)
        {
            if(read_tile(mario->coord.x - 1, mario->coord.y) < BRICK1)
            {
                switch(read_tile(mario->coord.x - 1, mario->coord.y))
                {
                    case COIN:
                        p_game->coins += 100;
                        write_score(p_game->coins);
                        write_tile(mario->coord.x - 1, mario->coord.y, BACKGROUND);
                        return TRUE;
                    case FLAG_3:
                    case FLAG_4:
                        animate_flag(mario);
                        return TRUE;
                    default:
                        return TRUE;
                }
            }
            else
            {
                switch(read_tile(mario->coord.x - 1, mario->coord.y))
                {
                    case MUSHROOM:
                        kill_mario(mario);
                        break;
                    case TURTLE_1:
                    case TURTLE_2:
                    case TURTLE_3:
                    case TURTLE_4:
                        kill_mario(mario);
                        break;
                }

                return FALSE;
            }
        }

        return FALSE;
    }
    else if(direction == UP)
    {
        if(read_tile(mario->coord.x, mario->coord.y - 3) == COINBLOCK_1)
        {
            write_tile(mario->coord.x, mario->coord.y-3, COINBLOCK_2);
            write_tile(mario->coord.x, mario->coord.y-4, COIN);
        }
        
        
        return 0x01;
    }
    else if(direction == DOWN)
    {
        P1 = mario->coord.y;
        if(read_tile(mario->coord.x, mario->coord.y + 1) >= BRICK1)
        {
            return FALSE;
        }

        switch(read_tile(mario->coord.x, mario->coord.y + 1))
        {
            case MUSHROOM:
                p_game->coins += 1000;
                write_score(p_game->coins);
                //P1 = numberActiveEnemies;
                for(unsigned char i = 0; i < numberActiveEnemies; i++)
                {
                    if(enemies[i].x == mario->coord.x && enemies[i].y == mario->coord.y + 1)
                    {
                        set_enemy_visibility(&enemies[i], FALSE);
                        return TRUE; //should always be reached
                    }
                }
                return TRUE;
            case COIN:
                p_game->coins += 100;
                write_score(p_game->coins);
                write_tile(mario->coord.x, mario->coord.y + 1, BACKGROUND);
                return TRUE;
            default:
                return TRUE;
        }

    }

    return FALSE;
}

void move_enemies(MARIO_S * mario)
{
    for(unsigned char i = 0; i < 4; i++)
    {
        if(enemies[i].visible == 0x1 && enemies[i].delay == DELAY)
        {
            enemies[i].delay = 0;
            move_enemy_direction(&enemies[i]);
            switch(enemies[i].direction)
            {
                case LEFT:
                    if(check_enemy_collisions(&enemies[i], LEFT) == TRUE)
                    {
                        --enemies[i].x;
                        if(enemies[i].size == 1)
                        {
                            write_tile(enemies[i].x ,enemies[i].y, enemies[i].TILE[0]);
                            write_tile(enemies[i].x+1, enemies[i].y, BACKGROUND);
                        }
                        else if(enemies[i].size == 2)
                        {
                            enemies[i].TILE[0] = TURTLE_1;
                            enemies[i].TILE[1] = TURTLE_2;
                             write_tile(enemies[i].x ,enemies[i].y, enemies[i].TILE[1]);
                             write_tile(enemies[i].x+1, enemies[i].y, BACKGROUND);
                             write_tile(enemies[i].x ,enemies[i].y-1, enemies[i].TILE[0]);
                             write_tile(enemies[i].x+1, enemies[i].y-1, BACKGROUND);
                        }
                        else
                        {
                            //should never reach here.
                        }
                    }
                    else if(check_enemy_collisions(&enemies[i], LEFT) == 0xFF)
                    {
                        kill_mario(mario);
                    }
                    else
                    {
                        set_enemy_direction(&enemies[i], RIGHT);
                        ++enemies[i].x;
                        if(enemies[i].size == 1)
                        {
                            write_tile(enemies[i].x ,enemies[i].y, enemies[i].TILE[0]);
                            write_tile(enemies[i].x-1, enemies[i].y, BACKGROUND);
                        }
                        else if(enemies[i].size == 2)
                        {
                            enemies[i].TILE[0] = TURTLE_3;
                            enemies[i].TILE[1] = TURTLE_4;
                            write_tile(enemies[i].x ,enemies[i].y, enemies[i].TILE[1]);
                            write_tile(enemies[i].x-1, enemies[i].y, BACKGROUND);
                            write_tile(enemies[i].x ,enemies[i].y-1, enemies[i].TILE[0]);
                            write_tile(enemies[i].x-1, enemies[i].y-1, BACKGROUND);
                        }
                        else
                        {
                            //should never reach here.
                        }
                    }

                    break;
                case RIGHT:
                    if(check_enemy_collisions(&enemies[i], RIGHT) == TRUE)
                    {
                        ++enemies[i].x;
                        if(enemies[i].size == 1)
                        {
                            write_tile(enemies[i].x ,enemies[i].y, enemies[i].TILE[0]);
                            write_tile(enemies[i].x-1, enemies[i].y, BACKGROUND);
                        }
                        else if(enemies[i].size == 2)
                        {
                            enemies[i].TILE[0] = TURTLE_3;
                            enemies[i].TILE[1] = TURTLE_4;
                            write_tile(enemies[i].x, enemies[i].y, enemies[i].TILE[1]);
                            write_tile(enemies[i].x-1, enemies[i].y, BACKGROUND);
                            write_tile(enemies[i].x, enemies[i].y-1, enemies[i].TILE[0]);
                            write_tile(enemies[i].x-1, enemies[i].y-1, BACKGROUND);
                        }
                        else
                        {
                            //should never reach here.
                        }
                    }
                    else if(check_enemy_collisions(&enemies[i], RIGHT) == KILL_MARIO)
                    {
                        kill_mario(mario);
                    }
                    else
                    {
                        set_enemy_direction(&enemies[i], LEFT);
                        --enemies[i].x;
                        if(enemies[i].size == 1)
                        {
                            write_tile(enemies[i].x ,enemies[i].y, enemies[i].TILE[0]);
                            write_tile(enemies[i].x+1, enemies[i].y, BACKGROUND);
                        }
                        else if(enemies[i].size == 2)
                        {
                            enemies[i].TILE[0] = TURTLE_1;
                            enemies[i].TILE[1] = TURTLE_2;
                            write_tile(enemies[i].x ,enemies[i].y, enemies[i].TILE[1]);
                            write_tile(enemies[i].x+1, enemies[i].y, BACKGROUND);
                            write_tile(enemies[i].x ,enemies[i].y-1, enemies[i].TILE[0]);
                            write_tile(enemies[i].x+1, enemies[i].y-1, BACKGROUND);
                        }
                        else
                        {
                            //should never reach here.
                        }

                    }
                break;
            }
            //move_tile(enemies[i].x, enemies[i].cart.y, enemies[i].TILE[0], LEFT);
            delay(0xFFFF);
        }
        else if(enemies[i].visible == TRUE && enemies[i].delay < DELAY)
        {
            enemies[i].delay++;
        }
    }
}

void kill_mario(MARIO_S * mario)
{
    /*unsigned char x = mario->coord.x;
    unsigned char y = mario->coord.y;

    write_tile(x,y,mario->TILE);
    for(unsigned char i = 0; i < 2; i++)
        delay(0xFFFF);

    write_tile(x,--y,mario->TILE);
    write_tile(x,y+1,BACKGROUND);
    for(unsigned char i = 0; i < 2; i++)
        delay(0xFFFF);

    write_tile(x,++y,mario->TILE);
    write_tile(x,y-1,BACKGROUND);
    for(unsigned char i = 0; i < 2; i++)
        delay(0xFFFF);

    write_tile(x,++y,mario->TILE);
    write_tile(x,y-1,BACKGROUND);
    for(unsigned char i = 0; i < 2; i++)
        delay(0xFFFF);

    write_tile(x,++y,mario->TILE);
    write_tile(x,y-1,BRICK1);
    for(unsigned char i = 0; i < 2; i++)
        delay(0xFFFF);

    write_tile(x,++y,mario->TILE);
    write_tile(x,y-1,BRICK1);
    for(unsigned char i = 0; i < 2; i++)
        delay(0xFFFF);

    write_tile(x,++y,mario->TILE);
    write_tile(x,y-1,BRICK1);
    for(unsigned char i = 0; i < 2; i++)
        delay(0xFFFF);

    if(games()->lives > 0)
    {
        games()->lives--;
        write_lives(games()->lives);
    }
    else
    {
        reset_game();
    }*/
}

GAME_S * games(void)
{
    return p_game;
}

void jump_mario(MARIO_S * mario)
{
    unsigned char x = mario->coord.x;
    unsigned char y = mario->coord.y;

    for(int i = 0; i < 3; i++)
    {
        move_tile_with_priority(mario->coord.x, mario->coord.y, mario->TILE, UP, 0x0);
        update_position(mario, x, y - 1);
        for(unsigned char j = 0; j < 2; j++)
            delay(0xFFFF);

        x = mario->coord.x;
        y = mario->coord.y;
    }

    for(int i = 0; i < 3; i++)
    {
        move_tile_with_priority(mario->coord.x, mario->coord.y, mario->TILE, DOWN, 0x0);
        update_position(mario, x, y + 1);
        for(unsigned char j = 0; j < 2; j++)
            delay(0xFFFF);

        x = mario->coord.x;
        y = mario->coord.y;
    }

    update_mario_jump(mario, FALSE);
}

BOOL check_enemy_collisions(ENEMY_S * enemy, unsigned char direction)
{
    switch(direction)
    {
        case LEFT:
            if(read_tile(enemy->x - 1, enemy->y) == BACKGROUND)
                return TRUE;
            else if(read_tile(enemy->x - 1, enemy->y) >= MARIO_1 && read_tile(enemy->x - 1, enemy->y) <= MARIO_7)
                return 0xFF;
            break;
        case RIGHT:
            if(read_tile(enemy->x + 1, enemy->y) == BACKGROUND)
                return TRUE;
            else if(read_tile(enemy->x + 1, enemy->y) >= MARIO_1 && read_tile(enemy->x + 1, enemy->y) <= MARIO_7)
                return KILL_MARIO;
            break;
    }

    return FALSE;
}

void init_enemies(void)
{
    /*for(unsigned char i = 0; i < 4; i++)
    {
        enemies[i].visible = FALSE;
    }*/
}

void animate_flag(MARIO_S * mario)
{
    if(p_game->flag == FALSE)
    {
        unsigned char tile = layers[0].TILE;
        unsigned char x;

        if(mario->direction == RIGHT)
        {
            x = mario->coord.x+1;
        }
        else
        {
            x = mario->coord.x-1;
        }

        unsigned char y = mario->coord.y;

        while(tile != FLAG_3)
        {
            tile = read_tile(x, --y);
        }

        unsigned char temp_flag_1 = BACKGROUND;
        unsigned char temp_flag_2 = FLAG_4;

        while(y != 27)
        {
            write_tile(x-1, y, temp_flag_1);
            write_tile(x, y, temp_flag_2);

            temp_flag_1 = read_tile(x-1,y+1);
            temp_flag_2 = read_tile(x, y+1);

            write_tile(x-1, y+1, FLAG_2);
            write_tile(x, y+1, FLAG_3);

            ++y;
            delay(0xFFFF);
        }

        p_game->coins+=10000;
        write_score(p_game->coins);
        p_game->flag = TRUE;
    }
}
