#include "hardware.h"

/*******************************************/
/*              aux window map             */
/* aux[0] = kbcontrol                      */
/* aux[1] = kbdata                         */
/* aux[2] = xyshift                        */
/* aux[3] = shift tile                     */
/* aux[4/5] = tonesequencer                  */
/*                                         */
/*                                         */
/*                                         */
/*******************************************/
__xdata unsigned char __xdata AUX[3] __at(0x1000);
__xdata unsigned char __xdata sound __at(0x1004);

unsigned char kb_release;

void write_memory(unsigned short index, unsigned char data)
{
    if(index < 64)
    {
        AUX[index] = data;
    }
}

unsigned char read_memory(unsigned short index)
{
    if(index < 64)
    {
        return AUX[index];
    }

    return 0xF1;
}

void init_hardware(void)
{
    init_interrupts();
    init_keyboard();
}

void delay(unsigned short cycles)
{
    for(unsigned short i = 0; i < cycles; i++)
                 __asm("nop");
}

void init_interrupts(void)
{
    // enable interrupts
    IE = 0x81;
    IP = 0x1;
    TCON = 0x1;
}

void init_keyboard(void)
{
    // reset keyboard
    write_memory(0, 1);
    write_memory(1, 0xFF);
    delay(0xFFFF);
    write_memory(0, 0x0);
}

__interrupt( __INTNO(0) )void keyboard_int( void )
{
    IE = 0x0;

    unsigned char key = read_memory(1);

    if(key == RELEASE)
    {
        kb_release = TRUE;
    }
    else if(key == KEY_LEFT && kb_release != TRUE)
    {
        add_button_queue(key);
    }
    else if(key == KEY_RIGHT && kb_release != TRUE)
    {
        add_button_queue(key);
    }
    else if(key == SPACE && kb_release != TRUE)
    {
        add_button_queue(key);
    }
    else if(key == KEY_DOWN && kb_release != TRUE)
    {
        add_button_queue(key);
    }
    else
    {
        kb_release = FALSE;
    }


    IE = 0x81;
}

void play_sound(unsigned char tone)
{
    sound = 0x1;
    write_memory(5, 1);
    delay(0xFF);
    sound = 0x1;
    write_memory(5, 0);
}






