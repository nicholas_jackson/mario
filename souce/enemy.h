#ifndef MARIO_ENEMY_H
#define MARIO_ENEMY_H

#include "mario.h"

#define DELAY 0x15
#define NUMENEMIES 4

typedef enum
{
    LEFT    = 0x0,
    RIGHT   = 0x1,
    STILL   = 0x2,
    UP      = 0x3,
    DOWN    = 0x4
}JUAN_DIRECTION ;

typedef struct
{
    unsigned char x;
    unsigned char y;
    unsigned char start_x;
    unsigned char start_y;
    unsigned char size;
    unsigned char TILE[2];
    unsigned char visible;
    unsigned char layer_priority;
    unsigned char distance;
    unsigned char direction;
    unsigned char delay;

} ENEMY_S;

void move_enemy_direction(ENEMY_S * enemy);
void set_enemy_visibility(ENEMY_S * enemy, unsigned char visibility);
void set_layer_priority(ENEMY_S * enemy, unsigned char layer);
void set_enemy_direction(ENEMY_S * enemy, unsigned char direction);
void kill_enemy(ENEMY_S * enemy);

#endif
