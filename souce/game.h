#ifndef MARIO_GAME_H
#define MARIO_GAME_H

#include "level.h"
#include "hardware.h"
#include "mario.h"
#include "enemy.h"

#define MAXQUEUE 8

typedef struct
{
    unsigned char size;
    unsigned char queue[MAXQUEUE];
} BUTTONQUEUE_S;

typedef struct
{                       
    unsigned char kb_hit;
    unsigned char kb_data;
    unsigned char last_kb_data;
} KEYBOARD_S;

void init_game(void);
void poll_button_queue(void);
void add_button_queue(unsigned char key);
void button_handler(unsigned char key);
void start_game(void);
void reset_game(void);
void detect_gap(void);

#endif
