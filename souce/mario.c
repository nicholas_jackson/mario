#include "mario.h"
#include "level.h"

void update_position(MARIO_S * mario, unsigned char x, unsigned char y)
{
    mario->coord.x = x;
    mario->coord.y = y;
}

void update_priority(MARIO_S * mario, unsigned char priority)
{
    mario->layer_priority = priority;
}

void update_mario_jump(MARIO_S * mario, BOOL jumping)
{
    mario->jumping = jumping;
}

BOOL get_mario_jump(MARIO_S * mario)
{
    return mario->jumping;
}

void update_mario_direction(MARIO_S * mario, unsigned char direction)
{
    switch(direction)
    {
        case LEFT:
            mario->TILE = MARIO_6;
            break;
        case RIGHT:
            mario->TILE = MARIO_2;
            break;
        default:
            mario->TILE = MARIO_2;
            break;
    }

    mario->direction = direction;
}


