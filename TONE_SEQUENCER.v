module tone_block(trigger, dataOut, clk, tone_sel);

       input trigger, clk;
       input reg [7:0] tone_sel;
       output reg [31:0] dataOut;
       reg [7:0] state;
       reg [31:0] count;
       reg [31:0] sequence [8:0];
       reg [31:0] tone [8:0];
       reg [31:0] i;
       reg [31:0] limit;

       initial
       begin
            i = 0;
            limit = 0;
       end

       always @(posedge clk)
       begin
            if(trigger)
                      state = 0;

            case(state)
            0:
            begin
            case(tone_sel)
            0:
            begin
                //coin
                sequence[0] = 200000;
                sequence[1] = 900000;
                tone[0] = 32'd101317;
                tone[1] = 32'd37921;
                limit = 2;
            end
            1:
            begin
                //jump
                sequence[0] = 80000;
                sequence[1] = 80000;
                sequence[2] = 20000;
                sequence[3] = 20000;
                sequence[4] = 20000;
                sequence[5] = 20000;
                sequence[6] = 20000;
                sequence[7] = 20000;
                sequence[8] = 20000;
                tone[0] = 32'd113636;
                tone[1] = 32'd227272;
                tone[2] = 32'd170357;
                tone[3] = 32'd151745;
                tone[4] = 32'd143266;
                tone[5] = 32'd127713;
                tone[6] = 32'd113636;
                tone[7] = 32'd101317;
                tone[8] = 32'd85178;
                limit = 9;
            end
            endcase
            i = 0;
            state = state + 1;
            end
            1:
            begin
                if(count < sequence[i])
                begin
                    dataOut = tone[i];
                    count = count + 1;
                end
                else
                begin
                    if(i < limit)
                    begin
                        count = 0;
                        i = i + 1;
                        dataOut = 0;
                    end
                    else
                        state = 2;

                end
            end
            2:
            begin
                dataOut = 0;
            end
            endcase
       end

endmodule
