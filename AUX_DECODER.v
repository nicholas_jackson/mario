module aux_addr_decoder(MEMADDR, MEMDATAI, MEMDATAO, MEMWR, CLK, RST, DATAO, CONTROL, DATAIN, SHIFT, TILE, TONEDATA, TONETRIGGER, NES_DATA);

    input CLK, RST, MEMWR;
    input [7:0] MEMDATAO;
    output reg [7:0] MEMDATAI;
    input reg [11:0] MEMADDR;
    output reg [7:0] DATAO;
    output reg [7:0] CONTROL;
    input [7:0] DATAIN;
    output reg [7:0] SHIFT;
    output reg [7:0] TILE;
    output reg [7:0] TONEDATA;
    output reg [7:0] TONETRIGGER;
    input reg [7:0] NES_DATA;

    always @(posedge CLK)
    begin
        if(MEMADDR == 0)   //kb
        begin
            CONTROL = MEMDATAO;
        end
        else if(MEMADDR == 1)  //kb
        begin
            DATAO = MEMDATAO;
            MEMDATAI = DATAIN;
        end
        else if(MEMADDR == 2)     //pixel shift
        begin
            SHIFT = MEMDATAO;
        end
        else if(MEMADDR == 3)    //pixel shift
        begin
            TILE = MEMDATAO;
        end
        else if(MEMADDR == 4)   //sound
        begin
            TONETRIGGER = MEMDATAO;
        end
        else if(MEMADDR == 5)   //sound
        begin
            TONEDATA = MEMDATAO;
        end
        else if(MEMADDR == 6)   //NES controller
        begin
            MEMDATAI = NES_DATA;
        end
    end

endmodule
