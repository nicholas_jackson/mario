------------------------------------------------------------
-- VHDL MULTIPLEX
-- 2013 10 23 21 1 29
-- Created By "Altium Designer VHDL Generator"
-- "Copyright (c) 2002-2004 Altium Limited"
------------------------------------------------------------

------------------------------------------------------------
-- VHDL MULTIPLEX
------------------------------------------------------------

Library IEEE;
Use     IEEE.std_logic_1164.all;

Entity MULTIPLEX Is
  port
  (
    MEMADDRI  : In    STD_LOGIC_VECTOR(15 DOWNTO 0);         -- ObjectKind=Port|PrimaryId=MEMADDRI[15..0]
    MEMADDRO  : Out   STD_LOGIC_VECTOR(15 DOWNTO 0);         -- ObjectKind=Port|PrimaryId=MEMADDRO[15..0]
    MEMDATAAO : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=MEMDATAAO[7..0]
    MEMDATABO : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=MEMDATABO[7..0]
    MEMDATACO : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=MEMDATACO[7..0]
    MEMDATAI  : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=MEMDATAI[7..0]
    MEMDATAIA : In    STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=MEMDATAIA[7..0]
    MEMDATAIB : In    STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=MEMDATAIB[7..0]
    MEMDATAIC : In    STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=MEMDATAIC[7..0]
    MEMDATAO  : In    STD_LOGIC_VECTOR(7 DOWNTO 0)           -- ObjectKind=Port|PrimaryId=MEMDATAO[7..0]
  );
  attribute MacroCell : boolean;


End MULTIPLEX;
------------------------------------------------------------

------------------------------------------------------------
architecture structure of MULTIPLEX is
   Component M8_B1B2                                         -- ObjectKind=Part|PrimaryId=DEMUX|SecondaryId=1
      port
      (
        A  : out STD_LOGIC_VECTOR(7 downto 0);               -- ObjectKind=Pin|PrimaryId=DEMUX-A[7..0]
        B  : out STD_LOGIC_VECTOR(7 downto 0);               -- ObjectKind=Pin|PrimaryId=DEMUX-B[7..0]
        S0 : in  STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=DEMUX-S0
        Y  : in  STD_LOGIC_VECTOR(7 downto 0)                -- ObjectKind=Pin|PrimaryId=DEMUX-Y[7..0]
      );
   End Component;

   Component M8_B2B1                                         -- ObjectKind=Part|PrimaryId=MUX|SecondaryId=1
      port
      (
        A  : in  STD_LOGIC_VECTOR(7 downto 0);               -- ObjectKind=Pin|PrimaryId=MUX-A[7..0]
        B  : in  STD_LOGIC_VECTOR(7 downto 0);               -- ObjectKind=Pin|PrimaryId=MUX-B[7..0]
        S0 : in  STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=MUX-S0
        Y  : out STD_LOGIC_VECTOR(7 downto 0)                -- ObjectKind=Pin|PrimaryId=MUX-Y[7..0]
      );
   End Component;


    Signal NamedSignal_MEMADDR  : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=MEMADDR[15..0]
    Signal PinSignal_DEMUX_A    : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetDEMUX2_Y[7..0]
    Signal PinSignal_DEMUX_B    : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=MEMDATACO[7..0]
    Signal PinSignal_DEMUX2_A   : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=MEMDATAAO[7..0]
    Signal PinSignal_DEMUX2_B   : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=MEMDATABO[7..0]
    Signal PinSignal_MUX_Y      : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=MEMDATAI[7..0]
    Signal PinSignal_MUX2_Y     : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetMUX2_Y[7..0]


begin
    MUX2 : M8_B2B1                                           -- ObjectKind=Part|PrimaryId=MUX2|SecondaryId=1
      Port Map
      (
        A  => MEMDATAIA,                                     -- ObjectKind=Pin|PrimaryId=MUX2-A[7..0]
        B  => MEMDATAIB,                                     -- ObjectKind=Pin|PrimaryId=MUX2-B[7..0]
        S0 => NamedSignal_MEMADDR(11),                       -- ObjectKind=Pin|PrimaryId=MUX2-S0
        Y  => PinSignal_MUX2_Y                               -- ObjectKind=Pin|PrimaryId=MUX2-Y[7..0]
      );

    MUX : M8_B2B1                                            -- ObjectKind=Part|PrimaryId=MUX|SecondaryId=1
      Port Map
      (
        A  => PinSignal_MUX2_Y,                              -- ObjectKind=Pin|PrimaryId=MUX-A[7..0]
        B  => MEMDATAIC,                                     -- ObjectKind=Pin|PrimaryId=MUX-B[7..0]
        S0 => NamedSignal_MEMADDR(12),                       -- ObjectKind=Pin|PrimaryId=MUX-S0
        Y  => PinSignal_MUX_Y                                -- ObjectKind=Pin|PrimaryId=MUX-Y[7..0]
      );

    DEMUX2 : M8_B1B2                                         -- ObjectKind=Part|PrimaryId=DEMUX2|SecondaryId=1
      Port Map
      (
        A  => PinSignal_DEMUX2_A,                            -- ObjectKind=Pin|PrimaryId=DEMUX2-A[7..0]
        B  => PinSignal_DEMUX2_B,                            -- ObjectKind=Pin|PrimaryId=DEMUX2-B[7..0]
        S0 => NamedSignal_MEMADDR(11),                       -- ObjectKind=Pin|PrimaryId=DEMUX2-S0
        Y  => PinSignal_DEMUX_A                              -- ObjectKind=Pin|PrimaryId=DEMUX2-Y[7..0]
      );

    DEMUX : M8_B1B2                                          -- ObjectKind=Part|PrimaryId=DEMUX|SecondaryId=1
      Port Map
      (
        A  => PinSignal_DEMUX_A,                             -- ObjectKind=Pin|PrimaryId=DEMUX-A[7..0]
        B  => PinSignal_DEMUX_B,                             -- ObjectKind=Pin|PrimaryId=DEMUX-B[7..0]
        S0 => NamedSignal_MEMADDR(12),                       -- ObjectKind=Pin|PrimaryId=DEMUX-S0
        Y  => MEMDATAO                                       -- ObjectKind=Pin|PrimaryId=DEMUX-Y[7..0]
      );

    -- Signal Assignments
    ---------------------
    MEMADDRO            <= NamedSignal_MEMADDR; -- ObjectKind=Net|PrimaryId=MEMADDR[15..0]
    MEMDATAAO           <= PinSignal_DEMUX2_A; -- ObjectKind=Net|PrimaryId=MEMDATAAO[7..0]
    MEMDATABO           <= PinSignal_DEMUX2_B; -- ObjectKind=Net|PrimaryId=MEMDATABO[7..0]
    MEMDATACO           <= PinSignal_DEMUX_B; -- ObjectKind=Net|PrimaryId=MEMDATACO[7..0]
    MEMDATAI            <= PinSignal_MUX_Y; -- ObjectKind=Net|PrimaryId=MEMDATAI[7..0]
    NamedSignal_MEMADDR <= MEMADDRI; -- ObjectKind=Net|PrimaryId=MEMADDR[15..0]

end structure;
------------------------------------------------------------

