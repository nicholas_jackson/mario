------------------------------------------------------------
-- VHDL AUX_BLOCK
-- 2013 10 23 21 1 29
-- Created By "Altium Designer VHDL Generator"
-- "Copyright (c) 2002-2004 Altium Limited"
------------------------------------------------------------

------------------------------------------------------------
-- VHDL AUX_BLOCK
------------------------------------------------------------

Library IEEE;
Use     IEEE.std_logic_1164.all;

Entity AUX_BLOCK Is
  port
  (
    ADDRI           : In    STD_LOGIC_VECTOR(15 DOWNTO 0);   -- ObjectKind=Port|PrimaryId=ADDRI[15..0]
    ADDRO           : Out   STD_LOGIC_VECTOR(15 DOWNTO 0);   -- ObjectKind=Port|PrimaryId=ADDRO[15..0]
    CLK             : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=CLK
    INT             : Out   STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=INT
    JTAG_NEXUS_TCK  : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TCK
    JTAG_NEXUS_TDI  : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TDI
    JTAG_NEXUS_TDO  : Out   STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TDO
    JTAG_NEXUS_TMS  : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TMS
    JTAG_NEXUS_TRST : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TRST
    MEMADDR         : In    STD_LOGIC_VECTOR(15 DOWNTO 0);   -- ObjectKind=Port|PrimaryId=MEMADDR[15..0]
    MEMDATAI        : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);    -- ObjectKind=Port|PrimaryId=MEMDATAI[7..0]
    MEMDATAO        : In    STD_LOGIC_VECTOR(7 DOWNTO 0);    -- ObjectKind=Port|PrimaryId=MEMDATAO[7..0]
    MEMWR           : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=MEMWR
    PS2B_CLK        : InOut STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=PS2B_CLK
    PS2B_DATA       : InOut STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=PS2B_DATA
    RST             : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=RST
    SPEAKER         : Out   STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=SPEAKER
    USER            : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=USER
    USERH_DATA      : In    STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=USERH_DATA
    USERH_LATCH     : Out   STD_LOGIC;                       -- ObjectKind=Port|PrimaryId=USERH_LATCH
    USERH_PULSE     : Out   STD_LOGIC                        -- ObjectKind=Port|PrimaryId=USERH_PULSE
  );
  attribute MacroCell : boolean;


End AUX_BLOCK;
------------------------------------------------------------

------------------------------------------------------------
architecture structure of AUX_BLOCK is
   Component aux_addr_decoder                                -- ObjectKind=Sheet Symbol|PrimaryId=U_aux_addr_decoder
      port
      (
        CLK         : in  STD_LOGIC;                         -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-CLK
        CONTROL     : out STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-CONTROL[7..0]
        DATAIN      : in  STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-DATAIN[7..0]
        DATAO       : out STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-DATAO[7..0]
        MEMADDR     : in  STD_LOGIC_VECTOR(11 downto 0);     -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMADDR[11..0]
        MEMDATAI    : out STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMDATAI[7..0]
        MEMDATAO    : in  STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMDATAO[7..0]
        MEMWR       : in  STD_LOGIC;                         -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMWR
        NES_DATA    : in  STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-NES_DATA[7..0]
        RST         : in  STD_LOGIC;                         -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-RST
        SHIFT       : out STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-SHIFT[7..0]
        TILE        : out STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-TILE[7..0]
        TONEDATA    : out STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-TONEDATA[7..0]
        TONETRIGGER : out STD_LOGIC_VECTOR(7 downto 0)       -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-TONETRIGGER[7..0]
      );
   End Component;

   Component CDIV2DC50                                       -- ObjectKind=Part|PrimaryId=cdiv4|SecondaryId=1
      port
      (
        CLKDV : out STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=cdiv4-CLKDV
        CLKIN : in  STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=cdiv4-CLKIN
      );
   End Component;

   Component CDIV3                                           -- ObjectKind=Part|PrimaryId=div1|SecondaryId=1
      port
      (
        CLKDV : out STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=div1-CLKDV
        CLKIN : in  STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=div1-CLKIN
      );
   End Component;

   Component CDIV10DC50                                      -- ObjectKind=Part|PrimaryId=div2|SecondaryId=1
      port
      (
        CLKDV : out STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=div2-CLKDV
        CLKIN : in  STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=div2-CLKIN
      );
   End Component;

   Component Configurable_io                                 -- ObjectKind=Part|PrimaryId=io|SecondaryId=1
      port
      (
        AIN  : in  STD_LOGIC_VECTOR(7 downto 0);             -- ObjectKind=Pin|PrimaryId=io-AIN[7..0]
        AOUT : out STD_LOGIC_VECTOR(7 downto 0);             -- ObjectKind=Pin|PrimaryId=io-AOUT[7..0]
        BIN  : in  STD_LOGIC_VECTOR(7 downto 0);             -- ObjectKind=Pin|PrimaryId=io-BIN[7..0]
        TCK  : in  STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=io-TCK
        TDI  : in  STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=io-TDI
        TDO  : out STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=io-TDO
        TMS  : in  STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=io-TMS
        TRST : in  STD_LOGIC                                 -- ObjectKind=Pin|PrimaryId=io-TRST
      );
   End Component;

   Component INV                                             -- ObjectKind=Part|PrimaryId=INV1|SecondaryId=1
      port
      (
        I : in  STD_LOGIC;                                   -- ObjectKind=Pin|PrimaryId=INV1-I
        O : out STD_LOGIC                                    -- ObjectKind=Pin|PrimaryId=INV1-O
      );
   End Component;

   Component nes_stub                                        -- ObjectKind=Sheet Symbol|PrimaryId=U_nes_stub
      port
      (
        clk             : in  STD_LOGIC;                     -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-clk
        controller_data : out STD_LOGIC_VECTOR(7 downto 0);  -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-controller_data[7..0]
        data            : in  STD_LOGIC;                     -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-data
        iclk            : in  STD_LOGIC;                     -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-iclk
        latch           : out STD_LOGIC;                     -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-latch
        pulse           : out STD_LOGIC;                     -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-pulse
        rst             : in  STD_LOGIC;                     -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-rst
        state           : out STD_LOGIC_VECTOR(7 downto 0)   -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-state[7..0]
      );
   End Component;

   Component pixel_shift                                     -- ObjectKind=Sheet Symbol|PrimaryId=U_pixel_shift
      port
      (
        addri  : in  STD_LOGIC_VECTOR(15 downto 0);          -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-addri[15..0]
        addro  : out STD_LOGIC_VECTOR(15 downto 0);          -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-addro[15..0]
        clk    : in  STD_LOGIC;                              -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-clk
        tile   : in  STD_LOGIC_VECTOR(7 downto 0);           -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-tile[7..0]
        xshift : in  STD_LOGIC_VECTOR(3 downto 0);           -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-xshift[3..0]
        yshift : in  STD_LOGIC_VECTOR(3 downto 0)            -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-yshift[3..0]
      );
   End Component;

   Component PS2_BLOCK                                       -- ObjectKind=Sheet Symbol|PrimaryId=U_PS2_BLOCK
      port
      (
        CLK       : in    STD_LOGIC;                         -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-CLK
        CONTROL   : in    STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-CONTROL[7..0]
        DATAI     : in    STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-DATAI[7..0]
        DATAO     : out   STD_LOGIC_VECTOR(7 downto 0);      -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-DATAO[7..0]
        INT       : out   STD_LOGIC;                         -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-INT
        PS2B_CLK  : inout STD_LOGIC;                         -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-PS2B_CLK
        PS2B_DATA : inout STD_LOGIC;                         -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-PS2B_DATA
        RST       : in    STD_LOGIC                          -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-RST
      );
   End Component;

   Component TONE_SHEET                                      -- ObjectKind=Sheet Symbol|PrimaryId=U_TONE_SHEET
      port
      (
        CLK     : in  STD_LOGIC;                             -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-CLK
        DATAIN  : in  STD_LOGIC_VECTOR(7 downto 0);          -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-DATAIN[7..0]
        SPEAKER : out STD_LOGIC;                             -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-SPEAKER
        TRIGGER : in  STD_LOGIC_VECTOR(7 downto 0);          -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-TRIGGER[7..0]
        USER    : in  STD_LOGIC                              -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-USER
      );
   End Component;


    Signal NamedSignal_JoinA_AUX_BLOCK_JB_C1        : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=MEMADDR[15..0]
    Signal NamedSignal_JoinB_AUX_BLOCK_JB_C1        : STD_LOGIC_VECTOR(11 downto 0); -- ObjectKind=Net|PrimaryId=MEMADDR
    Signal NamedSignal_SHIFT                        : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=SHIFT[3..0]
    Signal PinSignal_cdiv4_CLKDV                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netcdiv4_CLKDV
    Signal PinSignal_div1_CLKDV                     : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netdiv1_CLKDV
    Signal PinSignal_div2_CLKDV                     : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netdiv2_CLKDV
    Signal PinSignal_div3_CLKDV                     : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netcdiv4_CLKIN
    Signal PinSignal_INV1_O                         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetINV1_O
    Signal PinSignal_io_TDO                         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netio_TDO
    Signal PinSignal_U_aux_addr_decoder_CONTROL     : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=CONTROL
    Signal PinSignal_U_aux_addr_decoder_DATAO       : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=DATAO
    Signal PinSignal_U_aux_addr_decoder_MEMDATAI    : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=MEMDATAI[7..0]
    Signal PinSignal_U_aux_addr_decoder_SHIFT       : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=SHIFT[7..0]
    Signal PinSignal_U_aux_addr_decoder_TILE        : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=TILE
    Signal PinSignal_U_aux_addr_decoder_TONEDATA    : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=Netio_AIN[7..0]
    Signal PinSignal_U_aux_addr_decoder_TONETRIGGER : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=Netio_BIN[7..0]
    Signal PinSignal_U_nes_stub_controller_data     : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NES_DATA
    Signal PinSignal_U_nes_stub_latch               : STD_LOGIC; -- ObjectKind=Net|PrimaryId=latch
    Signal PinSignal_U_nes_stub_pulse               : STD_LOGIC; -- ObjectKind=Net|PrimaryId=pulse
    Signal PinSignal_U_pixel_shift_addro            : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=ADDRO[15..0]
    Signal PinSignal_U_pixel_shift_yshift           : STD_LOGIC_VECTOR(3 downto 0); -- ObjectKind=Net|PrimaryId=SHIFT[7..0]
    Signal PinSignal_U_PS2_BLOCK_DATAO              : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=DATAIN
    Signal PinSignal_U_PS2_BLOCK_INT                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=INT
    Signal PinSignal_U_TONE_SHEET_SPEAKER           : STD_LOGIC; -- ObjectKind=Net|PrimaryId=SPEAKER

   attribute VERILOGMODULE : string;
   attribute VERILOGMODULE of U_pixel_shift      : Label is "pixel_shift";
   attribute VERILOGMODULE of U_nes_stub         : Label is "nes_stub";
   attribute VERILOGMODULE of U_aux_addr_decoder : Label is "aux_addr_decoder";


begin
    U_TONE_SHEET : TONE_SHEET                                -- ObjectKind=Sheet Symbol|PrimaryId=U_TONE_SHEET
      Port Map
      (
        CLK     => CLK,                                      -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-CLK
        DATAIN  => PinSignal_U_aux_addr_decoder_TONETRIGGER, -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-DATAIN[7..0]
        SPEAKER => PinSignal_U_TONE_SHEET_SPEAKER,           -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-SPEAKER
        TRIGGER => PinSignal_U_aux_addr_decoder_TONETRIGGER, -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-TRIGGER[7..0]
        USER    => USER                                      -- ObjectKind=Sheet Entry|PrimaryId=TONE_SHEET.SchDoc-USER
      );

    U_PS2_BLOCK : PS2_BLOCK                                  -- ObjectKind=Sheet Symbol|PrimaryId=U_PS2_BLOCK
      Port Map
      (
        CLK       => CLK,                                    -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-CLK
        CONTROL   => PinSignal_U_aux_addr_decoder_CONTROL,   -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-CONTROL[7..0]
        DATAI     => PinSignal_U_aux_addr_decoder_DATAO,     -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-DATAI[7..0]
        DATAO     => PinSignal_U_PS2_BLOCK_DATAO,            -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-DATAO[7..0]
        INT       => PinSignal_U_PS2_BLOCK_INT,              -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-INT
        PS2B_CLK  => PS2B_CLK,                               -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-PS2B_CLK
        PS2B_DATA => PS2B_DATA,                              -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-PS2B_DATA
        RST       => RST                                     -- ObjectKind=Sheet Entry|PrimaryId=PS2_BLOCK.SchDoc-RST
      );

    U_pixel_shift : pixel_shift                              -- ObjectKind=Sheet Symbol|PrimaryId=U_pixel_shift
      Port Map
      (
        addri  => ADDRI,                                     -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-addri[15..0]
        addro  => PinSignal_U_pixel_shift_addro,             -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-addro[15..0]
        clk    => CLK,                                       -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-clk
        tile   => PinSignal_U_aux_addr_decoder_TILE,         -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-tile[7..0]
        xshift => NamedSignal_SHIFT(3 downto 0),             -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-xshift[3..0]
        yshift => PinSignal_U_pixel_shift_yshift             -- ObjectKind=Sheet Entry|PrimaryId=pixel_shift.v-yshift[3..0]
      );

    U_nes_stub : nes_stub                                    -- ObjectKind=Sheet Symbol|PrimaryId=U_nes_stub
      Port Map
      (
        clk             => PinSignal_cdiv4_CLKDV,            -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-clk
        controller_data => PinSignal_U_nes_stub_controller_data, -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-controller_data[7..0]
        data            => USERH_DATA,                       -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-data
        iclk            => PinSignal_INV1_O,                 -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-iclk
        latch           => PinSignal_U_nes_stub_latch,       -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-latch
        pulse           => PinSignal_U_nes_stub_pulse,       -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-pulse
        rst             => RST                               -- ObjectKind=Sheet Entry|PrimaryId=NES_CONTROLLER.v-rst
      );

    U_aux_addr_decoder : aux_addr_decoder                    -- ObjectKind=Sheet Symbol|PrimaryId=U_aux_addr_decoder
      Port Map
      (
        CLK         => CLK,                                  -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-CLK
        CONTROL     => PinSignal_U_aux_addr_decoder_CONTROL, -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-CONTROL[7..0]
        DATAIN      => PinSignal_U_PS2_BLOCK_DATAO,          -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-DATAIN[7..0]
        DATAO       => PinSignal_U_aux_addr_decoder_DATAO,   -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-DATAO[7..0]
        MEMADDR     => NamedSignal_JoinB_AUX_BLOCK_JB_C1,    -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMADDR[11..0]
        MEMDATAI    => PinSignal_U_aux_addr_decoder_MEMDATAI, -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMDATAI[7..0]
        MEMDATAO    => MEMDATAO,                             -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMDATAO[7..0]
        MEMWR       => MEMWR,                                -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-MEMWR
        NES_DATA    => PinSignal_U_nes_stub_controller_data, -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-NES_DATA[7..0]
        RST         => RST,                                  -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-RST
        SHIFT       => PinSignal_U_aux_addr_decoder_SHIFT,   -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-SHIFT[7..0]
        TILE        => PinSignal_U_aux_addr_decoder_TILE,    -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-TILE[7..0]
        TONEDATA    => PinSignal_U_aux_addr_decoder_TONEDATA, -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-TONEDATA[7..0]
        TONETRIGGER => PinSignal_U_aux_addr_decoder_TONETRIGGER -- ObjectKind=Sheet Entry|PrimaryId=AUX_DECODER.v-TONETRIGGER[7..0]
      );

    io : Configurable_io                                     -- ObjectKind=Part|PrimaryId=io|SecondaryId=1
      Port Map
      (
        AIN  => PinSignal_U_aux_addr_decoder_TONEDATA,       -- ObjectKind=Pin|PrimaryId=io-AIN[7..0]
        BIN  => PinSignal_U_aux_addr_decoder_TONETRIGGER,    -- ObjectKind=Pin|PrimaryId=io-BIN[7..0]
        TCK  => JTAG_NEXUS_TCK,                              -- ObjectKind=Pin|PrimaryId=io-TCK
        TDI  => JTAG_NEXUS_TDI,                              -- ObjectKind=Pin|PrimaryId=io-TDI
        TDO  => PinSignal_io_TDO,                            -- ObjectKind=Pin|PrimaryId=io-TDO
        TMS  => JTAG_NEXUS_TMS,                              -- ObjectKind=Pin|PrimaryId=io-TMS
        TRST => JTAG_NEXUS_TRST                              -- ObjectKind=Pin|PrimaryId=io-TRST
      );

    INV1 : INV                                               -- ObjectKind=Part|PrimaryId=INV1|SecondaryId=1
      Port Map
      (
        I => PinSignal_cdiv4_CLKDV,                          -- ObjectKind=Pin|PrimaryId=INV1-I
        O => PinSignal_INV1_O                                -- ObjectKind=Pin|PrimaryId=INV1-O
      );

    div3 : CDIV10DC50                                        -- ObjectKind=Part|PrimaryId=div3|SecondaryId=1
      Port Map
      (
        CLKDV => PinSignal_div3_CLKDV,                       -- ObjectKind=Pin|PrimaryId=div3-CLKDV
        CLKIN => PinSignal_div2_CLKDV                        -- ObjectKind=Pin|PrimaryId=div3-CLKIN
      );

    div2 : CDIV10DC50                                        -- ObjectKind=Part|PrimaryId=div2|SecondaryId=1
      Port Map
      (
        CLKDV => PinSignal_div2_CLKDV,                       -- ObjectKind=Pin|PrimaryId=div2-CLKDV
        CLKIN => PinSignal_div1_CLKDV                        -- ObjectKind=Pin|PrimaryId=div2-CLKIN
      );

    div1 : CDIV3                                             -- ObjectKind=Part|PrimaryId=div1|SecondaryId=1
      Port Map
      (
        CLKDV => PinSignal_div1_CLKDV,                       -- ObjectKind=Pin|PrimaryId=div1-CLKDV
        CLKIN => CLK                                         -- ObjectKind=Pin|PrimaryId=div1-CLKIN
      );

    cdiv4 : CDIV2DC50                                        -- ObjectKind=Part|PrimaryId=cdiv4|SecondaryId=1
      Port Map
      (
        CLKDV => PinSignal_cdiv4_CLKDV,                      -- ObjectKind=Pin|PrimaryId=cdiv4-CLKDV
        CLKIN => PinSignal_div3_CLKDV                        -- ObjectKind=Pin|PrimaryId=cdiv4-CLKIN
      );

    -- Signal Assignments
    ---------------------
    ADDRO                             <= PinSignal_U_pixel_shift_addro; -- ObjectKind=Net|PrimaryId=ADDRO[15..0]
    INT                               <= PinSignal_U_PS2_BLOCK_INT; -- ObjectKind=Net|PrimaryId=INT
    JTAG_NEXUS_TDO                    <= PinSignal_io_TDO; -- ObjectKind=Net|PrimaryId=Netio_TDO
    MEMDATAI                          <= PinSignal_U_aux_addr_decoder_MEMDATAI; -- ObjectKind=Net|PrimaryId=MEMDATAI[7..0]
    NamedSignal_JoinA_AUX_BLOCK_JB_C1 <= MEMADDR; -- ObjectKind=Net|PrimaryId=MEMADDR[15..0]
    NamedSignal_JoinB_AUX_BLOCK_JB_C1 <= NamedSignal_JoinA_AUX_BLOCK_JB_C1(11 downto 0);
    NamedSignal_SHIFT                 <= PinSignal_U_aux_addr_decoder_SHIFT; -- ObjectKind=Net|PrimaryId=SHIFT[7..0]
    PinSignal_U_pixel_shift_yshift    <= NamedSignal_SHIFT(7 downto 4); -- ObjectKind=Net|PrimaryId=SHIFT[7..0]
    SPEAKER                           <= PinSignal_U_TONE_SHEET_SPEAKER; -- ObjectKind=Net|PrimaryId=SPEAKER
    USERH_LATCH                       <= PinSignal_U_nes_stub_latch; -- ObjectKind=Net|PrimaryId=latch
    USERH_PULSE                       <= PinSignal_U_nes_stub_pulse; -- ObjectKind=Net|PrimaryId=pulse

end structure;
------------------------------------------------------------

