------------------------------------------------------------
-- VHDL main_schem
-- 2013 10 8 1 40 33
-- Created By "Altium Designer VHDL Generator"
-- "Copyright (c) 2002-2004 Altium Limited"
------------------------------------------------------------

------------------------------------------------------------
-- VHDL main_schem
------------------------------------------------------------

Library IEEE;
Use     IEEE.std_logic_1164.all;

Entity main_proj Is
  port
  (
    CLK_BRD        : In    STD_LOGIC;                        -- ObjectKind=Port|PrimaryId=CLK_BRD
    JTAG_NEXUS_TCK : In    STD_LOGIC;                        -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TCK
    JTAG_NEXUS_TDI : In    STD_LOGIC;                        -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TDI
    JTAG_NEXUS_TDO : Out   STD_LOGIC;                        -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TDO
    JTAG_NEXUS_TMS : In    STD_LOGIC;                        -- ObjectKind=Port|PrimaryId=JTAG_NEXUS_TMS
    LED_B          : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);     -- ObjectKind=Port|PrimaryId=LED_B[7..0]
    LED_G          : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);     -- ObjectKind=Port|PrimaryId=LED_G[7..0]
    LED_R          : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);     -- ObjectKind=Port|PrimaryId=LED_R[7..0]
    SW_USER0       : In    STD_LOGIC                         -- ObjectKind=Port|PrimaryId=SW_USER0
  );
  attribute MacroCell : boolean;

End main_proj;
------------------------------------------------------------

------------------------------------------------------------
architecture structure of main_proj is
   Component Configurable_U8                                 -- ObjectKind=Part|PrimaryId=U8|SecondaryId=1
      port
      (
        AIN  : in  STD_LOGIC_VECTOR(7 downto 0);             -- ObjectKind=Pin|PrimaryId=U8-AIN[7..0]
        AOUT : out STD_LOGIC_VECTOR(7 downto 0);             -- ObjectKind=Pin|PrimaryId=U8-AOUT[7..0]
        TCK  : in  STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=U8-TCK
        TDI  : in  STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=U8-TDI
        TDO  : out STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=U8-TDO
        TMS  : in  STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=U8-TMS
        TRST : in  STD_LOGIC                                 -- ObjectKind=Pin|PrimaryId=U8-TRST
      );
   End Component;

   Component FPGA_STARTUP8                                   -- ObjectKind=Part|PrimaryId=U3|SecondaryId=1
      port
      (
        CLK   : in  STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=U3-CLK
        DELAY : in  STD_LOGIC_VECTOR(7 downto 0);            -- ObjectKind=Pin|PrimaryId=U3-DELAY[7..0]
        INIT  : out STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=U3-INIT
      );
   End Component;

   Component INV                                             -- ObjectKind=Part|PrimaryId=U5|SecondaryId=1
      port
      (
        I : in  STD_LOGIC;                                   -- ObjectKind=Pin|PrimaryId=U5-I
        O : out STD_LOGIC                                    -- ObjectKind=Pin|PrimaryId=U5-O
      );
   End Component;

   Component Memory_TILERAM                                  -- ObjectKind=Part|PrimaryId=TILERAM|SecondaryId=1
      port
      (
        ADDRA : in  STD_LOGIC_VECTOR(12 downto 0);           -- ObjectKind=Pin|PrimaryId=TILERAM-ADDRA[12..0]
        ADDRB : in  STD_LOGIC_VECTOR(12 downto 0);           -- ObjectKind=Pin|PrimaryId=TILERAM-ADDRB[12..0]
        CLKA  : in  STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=TILERAM-CLKA
        CLKB  : in  STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=TILERAM-CLKB
        DINA  : in  STD_LOGIC_VECTOR(7 downto 0);            -- ObjectKind=Pin|PrimaryId=TILERAM-DINA[7..0]
        DINB  : in  STD_LOGIC_VECTOR(7 downto 0);            -- ObjectKind=Pin|PrimaryId=TILERAM-DINB[7..0]
        DOUTA : out STD_LOGIC_VECTOR(7 downto 0);            -- ObjectKind=Pin|PrimaryId=TILERAM-DOUTA[7..0]
        DOUTB : out STD_LOGIC_VECTOR(7 downto 0);            -- ObjectKind=Pin|PrimaryId=TILERAM-DOUTB[7..0]
        WEA   : in  STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=TILERAM-WEA
        WEB   : in  STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=TILERAM-WEB
      );
   End Component;

   Component Memory_U2                                       -- ObjectKind=Part|PrimaryId=U2|SecondaryId=1
      port
      (
        ADDR : in  STD_LOGIC_VECTOR(9 downto 0);             -- ObjectKind=Pin|PrimaryId=U2-ADDR[9..0]
        CLK  : in  STD_LOGIC;                                -- ObjectKind=Pin|PrimaryId=U2-CLK
        DIN  : in  STD_LOGIC_VECTOR(7 downto 0);             -- ObjectKind=Pin|PrimaryId=U2-DIN[7..0]
        DOUT : out STD_LOGIC_VECTOR(7 downto 0);             -- ObjectKind=Pin|PrimaryId=U2-DOUT[7..0]
        WE   : in  STD_LOGIC                                 -- ObjectKind=Pin|PrimaryId=U2-WE
      );
   End Component;

   Component OR2S                                            -- ObjectKind=Part|PrimaryId=U4|SecondaryId=1
      port
      (
        I0 : in  STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=U4-I0
        I1 : in  STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=U4-I1
        O  : out STD_LOGIC                                   -- ObjectKind=Pin|PrimaryId=U4-O
      );
   End Component;

   Component TSK51A_D                                        -- ObjectKind=Part|PrimaryId=U1|SecondaryId=1
      port
      (
        CLK      : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-CLK
        EA       : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-EA
        INT0     : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-INT0
        INT1     : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-INT1
        MEMADDR  : out STD_LOGIC_VECTOR(15 downto 0);        -- ObjectKind=Pin|PrimaryId=U1-MEMADDR[15..0]
        MEMDATAI : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-MEMDATAI[7..0]
        MEMDATAO : out STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-MEMDATAO[7..0]
        MEMRD    : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-MEMRD
        MEMWR    : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-MEMWR
        P0I      : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P0I[7..0]
        P0O      : out STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P0O[7..0]
        P1I      : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P1I[7..0]
        P1O      : out STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P1O[7..0]
        P2I      : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P2I[7..0]
        P2O      : out STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P2O[7..0]
        P3I      : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P3I[7..0]
        P3O      : out STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-P3O[7..0]
        PSRD     : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-PSRD
        PSWR     : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-PSWR
        ROMADDR  : out STD_LOGIC_VECTOR(15 downto 0);        -- ObjectKind=Pin|PrimaryId=U1-ROMADDR[15..0]
        ROMDATAI : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-ROMDATAI[7..0]
        ROMDATAO : out STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-ROMDATAO[7..0]
        ROMRD    : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-ROMRD
        ROMWR    : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-ROMWR
        RST      : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-RST
        RXD      : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-RXD
        RXDO     : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-RXDO
        SFRADDR  : out STD_LOGIC_VECTOR(6 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-SFRADDR[6..0]
        SFRDATAI : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-SFRDATAI[7..0]
        SFRDATAO : out STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Pin|PrimaryId=U1-SFRDATAO[7..0]
        SFRRD    : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-SFRRD
        SFRWR    : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-SFRWR
        T0       : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-T0
        T1       : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-T1
        TCK      : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-TCK
        TDI      : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-TDI
        TDO      : out STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-TDO
        TMS      : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-TMS
        TRST     : in  STD_LOGIC;                            -- ObjectKind=Pin|PrimaryId=U1-TRST
        TXD      : out STD_LOGIC                             -- ObjectKind=Pin|PrimaryId=U1-TXD
      );
   End Component;


    Signal NamedSignal_ADDRB                  : STD_LOGIC_VECTOR(12 downto 0); -- ObjectKind=Net|PrimaryId=ADDRB[12..0]
    Signal NamedSignal_GND1_BUS               : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=GND1_BUS[7..0]
    Signal NamedSignal_GND2_BUS               : STD_LOGIC_VECTOR(12 downto 8); -- ObjectKind=Net|PrimaryId=ADDRB[12..8]
    Signal NamedSignal_JoinA_main_schem_JB_C1 : STD_LOGIC_VECTOR(9 downto 0); -- ObjectKind=Net|PrimaryId=NetU2_ADDR[9..0]
    Signal NamedSignal_JoinA_main_schem_JB_C2 : STD_LOGIC_VECTOR(12 downto 0); -- ObjectKind=Net|PrimaryId=NetTILERAM_ADDRA[12..0]
    Signal NamedSignal_JoinB_main_schem_JB_C1 : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_ROMADDR[15..0]
    Signal NamedSignal_JoinB_main_schem_JB_C2 : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_MEMADDR[15..0]
    Signal NamedSignal_JTAG_NEXUS_TRST        : STD_LOGIC; -- ObjectKind=Net|PrimaryId=JTAG_NEXUS_TRST
    Signal NamedSignal_VCC1_BUS               : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=VCC1_BUS[7..0]
    Signal PinSignal_TILERAM_DOUTA            : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetTILERAM_DOUTA[7..0]
    Signal PinSignal_TILERAM_DOUTB            : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=LED_B[7..0]
    Signal PinSignal_U1_MEMADDR               : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_MEMADDR[15..0]
    Signal PinSignal_U1_MEMDATAO              : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetTILERAM_DINA[7..0]
    Signal PinSignal_U1_MEMWR                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILERAM_WEA
    Signal PinSignal_U1_P0O                   : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_P0I[7..0]
    Signal PinSignal_U1_P1O                   : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_P1I[7..0]
    Signal PinSignal_U1_P2O                   : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_P2I[7..0]
    Signal PinSignal_U1_P3O                   : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_P3I[7..0]
    Signal PinSignal_U1_ROMADDR               : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_ROMADDR[15..0]
    Signal PinSignal_U1_ROMDATAO              : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_ROMDATAO[7..0]
    Signal PinSignal_U1_ROMWR                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetU1_ROMWR
    Signal PinSignal_U1_SFRDATAO              : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_SFRDATAI[7..0]
    Signal PinSignal_U1_TDO                   : STD_LOGIC; -- ObjectKind=Net|PrimaryId=JTAG_NEXUS_LINK0
    Signal PinSignal_U2_DOUT                  : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=NetU1_ROMDATAI[7..0]
    Signal PinSignal_U3_INIT                  : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetU3_INIT
    Signal PinSignal_U4_O                     : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetU1_RST
    Signal PinSignal_U5_O                     : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetU4_I1
    Signal PinSignal_U6_O                     : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILERAM_CLKB
    Signal PinSignal_U8_AOUT                  : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=ADDRB[7..0]
    Signal PinSignal_U8_TDO                   : STD_LOGIC; -- ObjectKind=Net|PrimaryId=JTAG_NEXUS_TDO
    Signal PowerSignal_GND                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=GND
    Signal PowerSignal_VCC                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=JTAG_NEXUS_TRST

begin
    U8 : Configurable_U8                                     -- ObjectKind=Part|PrimaryId=U8|SecondaryId=1
      Port Map
      (
        AIN  => NamedSignal_GND1_BUS,                        -- ObjectKind=Pin|PrimaryId=U8-AIN[7..0]
        AOUT => PinSignal_U8_AOUT,                           -- ObjectKind=Pin|PrimaryId=U8-AOUT[7..0]
        TCK  => JTAG_NEXUS_TCK,                              -- ObjectKind=Pin|PrimaryId=U8-TCK
        TDI  => PinSignal_U1_TDO,                            -- ObjectKind=Pin|PrimaryId=U8-TDI
        TDO  => PinSignal_U8_TDO,                            -- ObjectKind=Pin|PrimaryId=U8-TDO
        TMS  => JTAG_NEXUS_TMS,                              -- ObjectKind=Pin|PrimaryId=U8-TMS
        TRST => NamedSignal_JTAG_NEXUS_TRST                  -- ObjectKind=Pin|PrimaryId=U8-TRST
      );

    U6 : INV                                                 -- ObjectKind=Part|PrimaryId=U6|SecondaryId=1
      Port Map
      (
        I => CLK_BRD,                                        -- ObjectKind=Pin|PrimaryId=U6-I
        O => PinSignal_U6_O                                  -- ObjectKind=Pin|PrimaryId=U6-O
      );

    U5 : INV                                                 -- ObjectKind=Part|PrimaryId=U5|SecondaryId=1
      Port Map
      (
        I => SW_USER0,                                       -- ObjectKind=Pin|PrimaryId=U5-I
        O => PinSignal_U5_O                                  -- ObjectKind=Pin|PrimaryId=U5-O
      );

    U4 : OR2S                                                -- ObjectKind=Part|PrimaryId=U4|SecondaryId=1
      Port Map
      (
        I0 => PinSignal_U3_INIT,                             -- ObjectKind=Pin|PrimaryId=U4-I0
        I1 => PinSignal_U5_O,                                -- ObjectKind=Pin|PrimaryId=U4-I1
        O  => PinSignal_U4_O                                 -- ObjectKind=Pin|PrimaryId=U4-O
      );

    U3 : FPGA_STARTUP8                                       -- ObjectKind=Part|PrimaryId=U3|SecondaryId=1
      Port Map
      (
        CLK   => CLK_BRD,                                    -- ObjectKind=Pin|PrimaryId=U3-CLK
        DELAY => NamedSignal_VCC1_BUS,                       -- ObjectKind=Pin|PrimaryId=U3-DELAY[7..0]
        INIT  => PinSignal_U3_INIT                           -- ObjectKind=Pin|PrimaryId=U3-INIT
      );

    U2 : Memory_U2                                           -- ObjectKind=Part|PrimaryId=U2|SecondaryId=1
      Port Map
      (
        ADDR => NamedSignal_JoinA_main_schem_JB_C1,          -- ObjectKind=Pin|PrimaryId=U2-ADDR[9..0]
        CLK  => CLK_BRD,                                     -- ObjectKind=Pin|PrimaryId=U2-CLK
        DIN  => PinSignal_U1_ROMDATAO,                       -- ObjectKind=Pin|PrimaryId=U2-DIN[7..0]
        DOUT => PinSignal_U2_DOUT,                           -- ObjectKind=Pin|PrimaryId=U2-DOUT[7..0]
        WE   => PinSignal_U1_ROMWR                           -- ObjectKind=Pin|PrimaryId=U2-WE
      );

    U1 : TSK51A_D                                            -- ObjectKind=Part|PrimaryId=U1|SecondaryId=1
      Port Map
      (
        CLK      => CLK_BRD,                                 -- ObjectKind=Pin|PrimaryId=U1-CLK
        EA       => PowerSignal_GND,                         -- ObjectKind=Pin|PrimaryId=U1-EA
        INT0     => PowerSignal_GND,                         -- ObjectKind=Pin|PrimaryId=U1-INT0
        INT1     => PowerSignal_GND,                         -- ObjectKind=Pin|PrimaryId=U1-INT1
        MEMADDR  => PinSignal_U1_MEMADDR,                    -- ObjectKind=Pin|PrimaryId=U1-MEMADDR[15..0]
        MEMDATAI => PinSignal_TILERAM_DOUTA,                 -- ObjectKind=Pin|PrimaryId=U1-MEMDATAI[7..0]
        MEMDATAO => PinSignal_U1_MEMDATAO,                   -- ObjectKind=Pin|PrimaryId=U1-MEMDATAO[7..0]
        MEMWR    => PinSignal_U1_MEMWR,                      -- ObjectKind=Pin|PrimaryId=U1-MEMWR
        P0I      => PinSignal_U1_P0O,                        -- ObjectKind=Pin|PrimaryId=U1-P0I[7..0]
        P0O      => PinSignal_U1_P0O,                        -- ObjectKind=Pin|PrimaryId=U1-P0O[7..0]
        P1I      => PinSignal_U1_P1O,                        -- ObjectKind=Pin|PrimaryId=U1-P1I[7..0]
        P1O      => PinSignal_U1_P1O,                        -- ObjectKind=Pin|PrimaryId=U1-P1O[7..0]
        P2I      => PinSignal_U1_P2O,                        -- ObjectKind=Pin|PrimaryId=U1-P2I[7..0]
        P2O      => PinSignal_U1_P2O,                        -- ObjectKind=Pin|PrimaryId=U1-P2O[7..0]
        P3I      => PinSignal_U1_P3O,                        -- ObjectKind=Pin|PrimaryId=U1-P3I[7..0]
        P3O      => PinSignal_U1_P3O,                        -- ObjectKind=Pin|PrimaryId=U1-P3O[7..0]
        ROMADDR  => PinSignal_U1_ROMADDR,                    -- ObjectKind=Pin|PrimaryId=U1-ROMADDR[15..0]
        ROMDATAI => PinSignal_U2_DOUT,                       -- ObjectKind=Pin|PrimaryId=U1-ROMDATAI[7..0]
        ROMDATAO => PinSignal_U1_ROMDATAO,                   -- ObjectKind=Pin|PrimaryId=U1-ROMDATAO[7..0]
        ROMWR    => PinSignal_U1_ROMWR,                      -- ObjectKind=Pin|PrimaryId=U1-ROMWR
        RST      => PinSignal_U4_O,                          -- ObjectKind=Pin|PrimaryId=U1-RST
        RXD      => PowerSignal_GND,                         -- ObjectKind=Pin|PrimaryId=U1-RXD
        SFRDATAI => PinSignal_U1_SFRDATAO,                   -- ObjectKind=Pin|PrimaryId=U1-SFRDATAI[7..0]
        SFRDATAO => PinSignal_U1_SFRDATAO,                   -- ObjectKind=Pin|PrimaryId=U1-SFRDATAO[7..0]
        T0       => PowerSignal_GND,                         -- ObjectKind=Pin|PrimaryId=U1-T0
        T1       => PowerSignal_GND,                         -- ObjectKind=Pin|PrimaryId=U1-T1
        TCK      => JTAG_NEXUS_TCK,                          -- ObjectKind=Pin|PrimaryId=U1-TCK
        TDI      => JTAG_NEXUS_TDI,                          -- ObjectKind=Pin|PrimaryId=U1-TDI
        TDO      => PinSignal_U1_TDO,                        -- ObjectKind=Pin|PrimaryId=U1-TDO
        TMS      => JTAG_NEXUS_TMS,                          -- ObjectKind=Pin|PrimaryId=U1-TMS
        TRST     => NamedSignal_JTAG_NEXUS_TRST              -- ObjectKind=Pin|PrimaryId=U1-TRST
      );

    TILERAM : Memory_TILERAM                                 -- ObjectKind=Part|PrimaryId=TILERAM|SecondaryId=1
      Port Map
      (
        ADDRA => NamedSignal_JoinA_main_schem_JB_C2,         -- ObjectKind=Pin|PrimaryId=TILERAM-ADDRA[12..0]
        ADDRB => NamedSignal_ADDRB,                          -- ObjectKind=Pin|PrimaryId=TILERAM-ADDRB[12..0]
        CLKA  => CLK_BRD,                                    -- ObjectKind=Pin|PrimaryId=TILERAM-CLKA
        CLKB  => PinSignal_U6_O,                             -- ObjectKind=Pin|PrimaryId=TILERAM-CLKB
        DINA  => PinSignal_U1_MEMDATAO,                      -- ObjectKind=Pin|PrimaryId=TILERAM-DINA[7..0]
        DINB  => PinSignal_TILERAM_DOUTB,                    -- ObjectKind=Pin|PrimaryId=TILERAM-DINB[7..0]
        DOUTA => PinSignal_TILERAM_DOUTA,                    -- ObjectKind=Pin|PrimaryId=TILERAM-DOUTA[7..0]
        DOUTB => PinSignal_TILERAM_DOUTB,                    -- ObjectKind=Pin|PrimaryId=TILERAM-DOUTB[7..0]
        WEA   => PinSignal_U1_MEMWR,                         -- ObjectKind=Pin|PrimaryId=TILERAM-WEA
        WEB   => PowerSignal_GND                             -- ObjectKind=Pin|PrimaryId=TILERAM-WEB
      );

    -- Signal Assignments
    ---------------------
    JTAG_NEXUS_TDO                     <= PinSignal_U8_TDO; -- ObjectKind=Net|PrimaryId=JTAG_NEXUS_TDO
    LED_B                              <= PinSignal_TILERAM_DOUTB; -- ObjectKind=Net|PrimaryId=LED_B[7..0]
    LED_G                              <= PinSignal_TILERAM_DOUTB; -- ObjectKind=Net|PrimaryId=LED_B[7..0]
    LED_R                              <= PinSignal_TILERAM_DOUTB; -- ObjectKind=Net|PrimaryId=LED_B[7..0]
    NamedSignal_ADDRB(12 downto 8)     <= NamedSignal_GND2_BUS; -- ObjectKind=Net|PrimaryId=ADDRB[12..8]
    NamedSignal_ADDRB(7 downto 0)      <= PinSignal_U8_AOUT; -- ObjectKind=Net|PrimaryId=ADDRB[7..0]
    NamedSignal_GND1_BUS               <= "00000000"; -- ObjectKind=Net|PrimaryId=GND1_BUS[7..0]
    NamedSignal_GND2_BUS               <= "00000"; -- ObjectKind=Net|PrimaryId=ADDRB[12..8]
    NamedSignal_JoinA_main_schem_JB_C1 <= NamedSignal_JoinB_main_schem_JB_C1(9 downto 0);
    NamedSignal_JoinA_main_schem_JB_C2 <= NamedSignal_JoinB_main_schem_JB_C2(12 downto 0);
    NamedSignal_JoinB_main_schem_JB_C1 <= PinSignal_U1_ROMADDR; -- ObjectKind=Net|PrimaryId=NetU1_ROMADDR[15..0]
    NamedSignal_JoinB_main_schem_JB_C2 <= PinSignal_U1_MEMADDR; -- ObjectKind=Net|PrimaryId=NetU1_MEMADDR[15..0]
    NamedSignal_JTAG_NEXUS_TRST        <= PowerSignal_VCC; -- ObjectKind=Net|PrimaryId=JTAG_NEXUS_TRST
    NamedSignal_VCC1_BUS               <= "11111111"; -- ObjectKind=Net|PrimaryId=VCC1_BUS[7..0]
    PowerSignal_GND                    <= '0'; -- ObjectKind=Net|PrimaryId=GND
    PowerSignal_VCC                    <= '1'; -- ObjectKind=Net|PrimaryId=JTAG_NEXUS_TRST

end structure;
------------------------------------------------------------

