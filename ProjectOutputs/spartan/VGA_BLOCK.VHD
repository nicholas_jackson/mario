------------------------------------------------------------
-- VHDL VGA_BLOCK
-- 2013 10 23 21 1 29
-- Created By "Altium Designer VHDL Generator"
-- "Copyright (c) 2002-2004 Altium Limited"
------------------------------------------------------------

------------------------------------------------------------
-- VHDL VGA_BLOCK
------------------------------------------------------------

Library IEEE;
Use     IEEE.std_logic_1164.all;

Entity VGA_BLOCK Is
  port
  (
    CLK      : In    STD_LOGIC;                              -- ObjectKind=Port|PrimaryId=CLK
    HSYNC    : Out   STD_LOGIC;                              -- ObjectKind=Port|PrimaryId=HSYNC
    RST      : In    STD_LOGIC;                              -- ObjectKind=Port|PrimaryId=RST
    VBLUE    : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);           -- ObjectKind=Port|PrimaryId=VBLUE[7..0]
    VGA_ADDR : Out   STD_LOGIC_VECTOR(18 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=VGA_ADDR[18..0]
    VGA_CLK  : Out   STD_LOGIC;                              -- ObjectKind=Port|PrimaryId=VGA_CLK
    VGA_DATA : In    STD_LOGIC_VECTOR(7 DOWNTO 0);           -- ObjectKind=Port|PrimaryId=VGA_DATA[7..0]
    VGREEN   : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);           -- ObjectKind=Port|PrimaryId=VGREEN[7..0]
    VRED     : Out   STD_LOGIC_VECTOR(7 DOWNTO 0);           -- ObjectKind=Port|PrimaryId=VRED[7..0]
    VSYNC    : Out   STD_LOGIC                               -- ObjectKind=Port|PrimaryId=VSYNC
  );
  attribute MacroCell : boolean;


End VGA_BLOCK;
------------------------------------------------------------

------------------------------------------------------------
architecture structure of VGA_BLOCK is
   Component CDIV2DC50                                       -- ObjectKind=Part|PrimaryId=div|SecondaryId=1
      port
      (
        CLKDV : out STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=div-CLKDV
        CLKIN : in  STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=div-CLKIN
      );
   End Component;

   Component VGA                                             -- ObjectKind=Part|PrimaryId=VGA_BLOCK|SecondaryId=1
      port
      (
        ADDR_PIXEL : out STD_LOGIC_VECTOR(18 downto 0);      -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-ADDR_PIXEL[18..0]
        B0         : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-B0
        B1         : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-B1
        CLK        : in  STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-CLK
        CMOD       : in  STD_LOGIC_VECTOR(1 downto 0);       -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-CMOD[1..0]
        DATA       : in  STD_LOGIC_VECTOR(7 downto 0);       -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-DATA[7..0]
        DISPSIZE_H : in  STD_LOGIC_VECTOR(9 downto 0);       -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-DISPSIZE_H[9..0]
        DISPSIZE_V : in  STD_LOGIC_VECTOR(9 downto 0);       -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-DISPSIZE_V[9..0]
        G0         : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-G0
        G1         : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-G1
        HSYNC      : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-HSYNC
        R0         : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-R0
        R1         : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-R1
        RD         : out STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-RD
        RESOLUTION : in  STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-RESOLUTION
        RST        : in  STD_LOGIC;                          -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-RST
        VSYNC      : out STD_LOGIC                           -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-VSYNC
      );
   End Component;


    Signal NamedSignal_CMOD               : STD_LOGIC_VECTOR(1 downto 0); -- ObjectKind=Net|PrimaryId=CMOD[1..0]
    Signal NamedSignal_DISPSIZE_H         : STD_LOGIC_VECTOR(9 downto 0); -- ObjectKind=Net|PrimaryId=DISPSIZE_H[9..0]
    Signal NamedSignal_DISPSIZE_V         : STD_LOGIC_VECTOR(9 downto 0); -- ObjectKind=Net|PrimaryId=DISPSIZE_V[9..0]
    Signal NamedSignal_GND1_BUS           : STD_LOGIC_VECTOR(5 downto 0); -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    Signal NamedSignal_VBLUE              : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    Signal NamedSignal_VGREEN             : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    Signal NamedSignal_VRED               : STD_LOGIC_VECTOR(7 downto 0); -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    Signal PinSignal_div_CLKDV            : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netdiv_CLKDV
    Signal PinSignal_VGA_BLOCK_ADDR_PIXEL : STD_LOGIC_VECTOR(18 downto 0); -- ObjectKind=Net|PrimaryId=VGA_ADDR[18..0]
    Signal PinSignal_VGA_BLOCK_B0         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=VBLUE6
    Signal PinSignal_VGA_BLOCK_B1         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=VBLUE7
    Signal PinSignal_VGA_BLOCK_G0         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=VGREEN6
    Signal PinSignal_VGA_BLOCK_G1         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=VGREEN7
    Signal PinSignal_VGA_BLOCK_HSYNC      : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetVGA_BLOCK_HSYNC
    Signal PinSignal_VGA_BLOCK_R0         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=VRED6
    Signal PinSignal_VGA_BLOCK_R1         : STD_LOGIC; -- ObjectKind=Net|PrimaryId=VRED7
    Signal PinSignal_VGA_BLOCK_VSYNC      : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetVGA_BLOCK_VSYNC
    Signal PowerSignal_GND                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=GND


begin
    VGA_BLOCK : VGA                                          -- ObjectKind=Part|PrimaryId=VGA_BLOCK|SecondaryId=1
      Port Map
      (
        ADDR_PIXEL => PinSignal_VGA_BLOCK_ADDR_PIXEL,        -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-ADDR_PIXEL[18..0]
        B0         => PinSignal_VGA_BLOCK_B0,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-B0
        B1         => PinSignal_VGA_BLOCK_B1,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-B1
        CLK        => PinSignal_div_CLKDV,                   -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-CLK
        CMOD       => NamedSignal_CMOD,                      -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-CMOD[1..0]
        DATA       => VGA_DATA,                              -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-DATA[7..0]
        DISPSIZE_H => NamedSignal_DISPSIZE_H,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-DISPSIZE_H[9..0]
        DISPSIZE_V => NamedSignal_DISPSIZE_V,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-DISPSIZE_V[9..0]
        G0         => PinSignal_VGA_BLOCK_G0,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-G0
        G1         => PinSignal_VGA_BLOCK_G1,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-G1
        HSYNC      => PinSignal_VGA_BLOCK_HSYNC,             -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-HSYNC
        R0         => PinSignal_VGA_BLOCK_R0,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-R0
        R1         => PinSignal_VGA_BLOCK_R1,                -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-R1
        RESOLUTION => PowerSignal_GND,                       -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-RESOLUTION
        RST        => RST,                                   -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-RST
        VSYNC      => PinSignal_VGA_BLOCK_VSYNC              -- ObjectKind=Pin|PrimaryId=VGA_BLOCK-VSYNC
      );

    div : CDIV2DC50                                          -- ObjectKind=Part|PrimaryId=div|SecondaryId=1
      Port Map
      (
        CLKDV => PinSignal_div_CLKDV,                        -- ObjectKind=Pin|PrimaryId=div-CLKDV
        CLKIN => CLK                                         -- ObjectKind=Pin|PrimaryId=div-CLKIN
      );

    -- Signal Assignments
    ---------------------
    HSYNC                          <= PinSignal_VGA_BLOCK_HSYNC; -- ObjectKind=Net|PrimaryId=NetVGA_BLOCK_HSYNC
    NamedSignal_CMOD               <= "10"; -- ObjectKind=Net|PrimaryId=CMOD[1..0]
    NamedSignal_DISPSIZE_H         <= "1000000000"; -- ObjectKind=Net|PrimaryId=DISPSIZE_H[9..0]
    NamedSignal_DISPSIZE_V         <= "1000000000"; -- ObjectKind=Net|PrimaryId=DISPSIZE_V[9..0]
    NamedSignal_GND1_BUS           <= "000000"; -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    NamedSignal_VBLUE(5 downto 0)  <= NamedSignal_GND1_BUS; -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    NamedSignal_VBLUE(6)           <= PinSignal_VGA_BLOCK_B0; -- ObjectKind=Net|PrimaryId=VBLUE6
    NamedSignal_VBLUE(7)           <= PinSignal_VGA_BLOCK_B1; -- ObjectKind=Net|PrimaryId=VBLUE7
    NamedSignal_VGREEN(5 downto 0) <= NamedSignal_GND1_BUS; -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    NamedSignal_VGREEN(6)          <= PinSignal_VGA_BLOCK_G0; -- ObjectKind=Net|PrimaryId=VGREEN6
    NamedSignal_VGREEN(7)          <= PinSignal_VGA_BLOCK_G1; -- ObjectKind=Net|PrimaryId=VGREEN7
    NamedSignal_VRED(5 downto 0)   <= NamedSignal_GND1_BUS; -- ObjectKind=Net|PrimaryId=GND1_BUS[5..0]
    NamedSignal_VRED(6)            <= PinSignal_VGA_BLOCK_R0; -- ObjectKind=Net|PrimaryId=VRED6
    NamedSignal_VRED(7)            <= PinSignal_VGA_BLOCK_R1; -- ObjectKind=Net|PrimaryId=VRED7
    PowerSignal_GND                <= '0'; -- ObjectKind=Net|PrimaryId=GND
    VBLUE                          <= NamedSignal_VBLUE; -- ObjectKind=Net|PrimaryId=VBLUE[7..0]
    VGA_ADDR                       <= PinSignal_VGA_BLOCK_ADDR_PIXEL; -- ObjectKind=Net|PrimaryId=VGA_ADDR[18..0]
    VGA_CLK                        <= PinSignal_div_CLKDV; -- ObjectKind=Net|PrimaryId=Netdiv_CLKDV
    VGREEN                         <= NamedSignal_VGREEN; -- ObjectKind=Net|PrimaryId=VGREEN[7..0]
    VRED                           <= NamedSignal_VRED; -- ObjectKind=Net|PrimaryId=VRED[7..0]
    VSYNC                          <= PinSignal_VGA_BLOCK_VSYNC; -- ObjectKind=Net|PrimaryId=NetVGA_BLOCK_VSYNC

end structure;
------------------------------------------------------------

