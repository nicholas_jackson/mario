-- -----------------------------------------------------------------
-- "Copyright (C) Altium Limited 2003"
-- -----------------------------------------------------------------
-- Component Name: 	J3B_3S
-- Description: 	3-Bit input bus to 3 Single pin outputs
-- Core Revision: 	1.00.00
-- -----------------------------------------------------------------
-- Modifications with respect to Version  : 
--
--
-- -----------------------------------------------------------------

library IEEE;
use IEEE.Std_Logic_1164.all;

entity J3B_3S is
  port (
    I : in std_logic_vector(2 downto 0);
    O0, O1, O2 : out std_logic
    );
end entity;

architecture STRUCTURE of J3B_3S is
begin

  O0 <= I(0);
  O1 <= I(1);
  O2 <= I(2);

end architecture;
