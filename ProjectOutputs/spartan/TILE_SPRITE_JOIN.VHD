------------------------------------------------------------
-- VHDL TILE_SPRITE_JOIN
-- 2013 10 23 21 1 29
-- Created By "Altium Designer VHDL Generator"
-- "Copyright (c) 2002-2004 Altium Limited"
------------------------------------------------------------

------------------------------------------------------------
-- VHDL TILE_SPRITE_JOIN
------------------------------------------------------------

Library IEEE;
Use     IEEE.std_logic_1164.all;

Entity TILE_SPRITE_JOIN Is
  port
  (
    SPRITE_I  : Out   STD_LOGIC_VECTOR(15 DOWNTO 0);         -- ObjectKind=Port|PrimaryId=SPRITE_I[15..0]
    SPRITE_O  : In    STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=SPRITE_O[7..0]
    TILEMAP_I : Out   STD_LOGIC_VECTOR(10 DOWNTO 0);         -- ObjectKind=Port|PrimaryId=TILEMAP_I[10..0]
    TILEMAP_O : In    STD_LOGIC_VECTOR(7 DOWNTO 0);          -- ObjectKind=Port|PrimaryId=TILEMAP_O[7..0]
    VGA_ADDR  : In    STD_LOGIC_VECTOR(18 DOWNTO 0);         -- ObjectKind=Port|PrimaryId=VGA_ADDR[18..0]
    VGA_DATA  : Out   STD_LOGIC_VECTOR(7 DOWNTO 0)           -- ObjectKind=Port|PrimaryId=VGA_DATA[7..0]
  );
  attribute MacroCell : boolean;


End TILE_SPRITE_JOIN;
------------------------------------------------------------

------------------------------------------------------------
architecture structure of TILE_SPRITE_JOIN is
   Component J3B_3S                                          -- ObjectKind=Part|PrimaryId=VGA_ADDR_HH|SecondaryId=1
      port
      (
        I  : in  STD_LOGIC_VECTOR(2 downto 0);               -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HH-I[2..0]
        O0 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HH-O0
        O1 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HH-O1
        O2 : out STD_LOGIC                                   -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HH-O2
      );
   End Component;

   Component J8B_8S                                          -- ObjectKind=Part|PrimaryId=TILEMAPO|SecondaryId=1
      port
      (
        I  : in  STD_LOGIC_VECTOR(7 downto 0);               -- ObjectKind=Pin|PrimaryId=TILEMAPO-I[7..0]
        O0 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=TILEMAPO-O0
        O1 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=TILEMAPO-O1
        O2 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=TILEMAPO-O2
        O3 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=TILEMAPO-O3
        O4 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=TILEMAPO-O4
        O5 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=TILEMAPO-O5
        O6 : out STD_LOGIC;                                  -- ObjectKind=Pin|PrimaryId=TILEMAPO-O6
        O7 : out STD_LOGIC                                   -- ObjectKind=Pin|PrimaryId=TILEMAPO-O7
      );
   End Component;

   Component J16B_16S                                        -- ObjectKind=Part|PrimaryId=VGA_ADDR_HL|SecondaryId=1
      port
      (
        I   : in  STD_LOGIC_VECTOR(15 downto 0);             -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-I[15..0]
        O0  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O0
        O1  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O1
        O2  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O2
        O3  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O3
        O4  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O4
        O5  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O5
        O6  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O6
        O7  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O7
        O8  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O8
        O9  : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O9
        O10 : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O10
        O11 : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O11
        O12 : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O12
        O13 : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O13
        O14 : out STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O14
        O15 : out STD_LOGIC                                  -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O15
      );
   End Component;

   Component J16S_16B                                        -- ObjectKind=Part|PrimaryId=SPRITEI|SecondaryId=1
      port
      (
        I0  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I0
        I1  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I1
        I2  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I2
        I3  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I3
        I4  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I4
        I5  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I5
        I6  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I6
        I7  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I7
        I8  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I8
        I9  : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I9
        I10 : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I10
        I11 : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I11
        I12 : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I12
        I13 : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I13
        I14 : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I14
        I15 : in  STD_LOGIC;                                 -- ObjectKind=Pin|PrimaryId=SPRITEI-I15
        O   : out STD_LOGIC_VECTOR(15 downto 0)              -- ObjectKind=Pin|PrimaryId=SPRITEI-O[15..0]
      );
   End Component;


    Signal NamedSignal_JoinA_TILE_SPRITE_JOIN_JB_C1 : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=NetTILEMAPI_O[15..0]
    Signal NamedSignal_JoinB_TILE_SPRITE_JOIN_JB_C1 : STD_LOGIC_VECTOR(10 downto 0); -- ObjectKind=Net|PrimaryId=TILEMAP_I[10..0]
    Signal NamedSignal_VGA_ADDR                     : STD_LOGIC_VECTOR(18 downto 0); -- ObjectKind=Net|PrimaryId=VGA_ADDR[15..0]
    Signal PinSignal_SPRITEI_O                      : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=SPRITE_I[15..0]
    Signal PinSignal_TILEMAPI_O                     : STD_LOGIC_VECTOR(15 downto 0); -- ObjectKind=Net|PrimaryId=NetTILEMAPI_O[15..0]
    Signal PinSignal_TILEMAPO_O0                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I8
    Signal PinSignal_TILEMAPO_O1                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I9
    Signal PinSignal_TILEMAPO_O2                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I10
    Signal PinSignal_TILEMAPO_O3                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I11
    Signal PinSignal_TILEMAPO_O4                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I12
    Signal PinSignal_TILEMAPO_O5                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I13
    Signal PinSignal_TILEMAPO_O6                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I14
    Signal PinSignal_TILEMAPO_O7                    : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I15
    Signal PinSignal_VGA_ADDR_HH_I                  : STD_LOGIC_VECTOR(2 downto 0); -- ObjectKind=Net|PrimaryId=VGA_ADDR[18..0]
    Signal PinSignal_VGA_ADDR_HH_O0                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I8
    Signal PinSignal_VGA_ADDR_HH_O1                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I9
    Signal PinSignal_VGA_ADDR_HL_O0                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I0
    Signal PinSignal_VGA_ADDR_HL_O1                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I1
    Signal PinSignal_VGA_ADDR_HL_O10                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I5
    Signal PinSignal_VGA_ADDR_HL_O11                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I6
    Signal PinSignal_VGA_ADDR_HL_O12                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I7
    Signal PinSignal_VGA_ADDR_HL_O13                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I5
    Signal PinSignal_VGA_ADDR_HL_O14                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I6
    Signal PinSignal_VGA_ADDR_HL_O15                : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I7
    Signal PinSignal_VGA_ADDR_HL_O2                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I2
    Signal PinSignal_VGA_ADDR_HL_O3                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I3
    Signal PinSignal_VGA_ADDR_HL_O4                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I0
    Signal PinSignal_VGA_ADDR_HL_O5                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I1
    Signal PinSignal_VGA_ADDR_HL_O6                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I2
    Signal PinSignal_VGA_ADDR_HL_O7                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I3
    Signal PinSignal_VGA_ADDR_HL_O8                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_I4
    Signal PinSignal_VGA_ADDR_HL_O9                 : STD_LOGIC; -- ObjectKind=Net|PrimaryId=NetSPRITEI_I4
    Signal PowerSignal_GND                          : STD_LOGIC; -- ObjectKind=Net|PrimaryId=GND


begin
    VGA_ADDR_HL : J16B_16S                                   -- ObjectKind=Part|PrimaryId=VGA_ADDR_HL|SecondaryId=1
      Port Map
      (
        I   => NamedSignal_VGA_ADDR(15 downto 0),            -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-I[15..0]
        O0  => PinSignal_VGA_ADDR_HL_O0,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O0
        O1  => PinSignal_VGA_ADDR_HL_O1,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O1
        O2  => PinSignal_VGA_ADDR_HL_O2,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O2
        O3  => PinSignal_VGA_ADDR_HL_O3,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O3
        O4  => PinSignal_VGA_ADDR_HL_O4,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O4
        O5  => PinSignal_VGA_ADDR_HL_O5,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O5
        O6  => PinSignal_VGA_ADDR_HL_O6,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O6
        O7  => PinSignal_VGA_ADDR_HL_O7,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O7
        O8  => PinSignal_VGA_ADDR_HL_O8,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O8
        O9  => PinSignal_VGA_ADDR_HL_O9,                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O9
        O10 => PinSignal_VGA_ADDR_HL_O10,                    -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O10
        O11 => PinSignal_VGA_ADDR_HL_O11,                    -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O11
        O12 => PinSignal_VGA_ADDR_HL_O12,                    -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O12
        O13 => PinSignal_VGA_ADDR_HL_O13,                    -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O13
        O14 => PinSignal_VGA_ADDR_HL_O14,                    -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O14
        O15 => PinSignal_VGA_ADDR_HL_O15                     -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HL-O15
      );

    VGA_ADDR_HH : J3B_3S                                     -- ObjectKind=Part|PrimaryId=VGA_ADDR_HH|SecondaryId=1
      Port Map
      (
        I  => PinSignal_VGA_ADDR_HH_I,                       -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HH-I[2..0]
        O0 => PinSignal_VGA_ADDR_HH_O0,                      -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HH-O0
        O1 => PinSignal_VGA_ADDR_HH_O1                       -- ObjectKind=Pin|PrimaryId=VGA_ADDR_HH-O1
      );

    TILEMAPO : J8B_8S                                        -- ObjectKind=Part|PrimaryId=TILEMAPO|SecondaryId=1
      Port Map
      (
        I  => TILEMAP_O,                                     -- ObjectKind=Pin|PrimaryId=TILEMAPO-I[7..0]
        O0 => PinSignal_TILEMAPO_O0,                         -- ObjectKind=Pin|PrimaryId=TILEMAPO-O0
        O1 => PinSignal_TILEMAPO_O1,                         -- ObjectKind=Pin|PrimaryId=TILEMAPO-O1
        O2 => PinSignal_TILEMAPO_O2,                         -- ObjectKind=Pin|PrimaryId=TILEMAPO-O2
        O3 => PinSignal_TILEMAPO_O3,                         -- ObjectKind=Pin|PrimaryId=TILEMAPO-O3
        O4 => PinSignal_TILEMAPO_O4,                         -- ObjectKind=Pin|PrimaryId=TILEMAPO-O4
        O5 => PinSignal_TILEMAPO_O5,                         -- ObjectKind=Pin|PrimaryId=TILEMAPO-O5
        O6 => PinSignal_TILEMAPO_O6,                         -- ObjectKind=Pin|PrimaryId=TILEMAPO-O6
        O7 => PinSignal_TILEMAPO_O7                          -- ObjectKind=Pin|PrimaryId=TILEMAPO-O7
      );

    TILEMAPI : J16S_16B                                      -- ObjectKind=Part|PrimaryId=TILEMAPI|SecondaryId=1
      Port Map
      (
        I0  => PinSignal_VGA_ADDR_HL_O4,                     -- ObjectKind=Pin|PrimaryId=TILEMAPI-I0
        I1  => PinSignal_VGA_ADDR_HL_O5,                     -- ObjectKind=Pin|PrimaryId=TILEMAPI-I1
        I2  => PinSignal_VGA_ADDR_HL_O6,                     -- ObjectKind=Pin|PrimaryId=TILEMAPI-I2
        I3  => PinSignal_VGA_ADDR_HL_O7,                     -- ObjectKind=Pin|PrimaryId=TILEMAPI-I3
        I4  => PinSignal_VGA_ADDR_HL_O8,                     -- ObjectKind=Pin|PrimaryId=TILEMAPI-I4
        I5  => PinSignal_VGA_ADDR_HL_O13,                    -- ObjectKind=Pin|PrimaryId=TILEMAPI-I5
        I6  => PinSignal_VGA_ADDR_HL_O14,                    -- ObjectKind=Pin|PrimaryId=TILEMAPI-I6
        I7  => PinSignal_VGA_ADDR_HL_O15,                    -- ObjectKind=Pin|PrimaryId=TILEMAPI-I7
        I8  => PinSignal_VGA_ADDR_HH_O0,                     -- ObjectKind=Pin|PrimaryId=TILEMAPI-I8
        I9  => PinSignal_VGA_ADDR_HH_O1,                     -- ObjectKind=Pin|PrimaryId=TILEMAPI-I9
        I10 => PowerSignal_GND,                              -- ObjectKind=Pin|PrimaryId=TILEMAPI-I10
        I11 => PowerSignal_GND,                              -- ObjectKind=Pin|PrimaryId=TILEMAPI-I11
        I12 => PowerSignal_GND,                              -- ObjectKind=Pin|PrimaryId=TILEMAPI-I12
        I13 => PowerSignal_GND,                              -- ObjectKind=Pin|PrimaryId=TILEMAPI-I13
        I14 => PowerSignal_GND,                              -- ObjectKind=Pin|PrimaryId=TILEMAPI-I14
        I15 => PowerSignal_GND,                              -- ObjectKind=Pin|PrimaryId=TILEMAPI-I15
        O   => PinSignal_TILEMAPI_O                          -- ObjectKind=Pin|PrimaryId=TILEMAPI-O[15..0]
      );

    SPRITEI : J16S_16B                                       -- ObjectKind=Part|PrimaryId=SPRITEI|SecondaryId=1
      Port Map
      (
        I0  => PinSignal_VGA_ADDR_HL_O0,                     -- ObjectKind=Pin|PrimaryId=SPRITEI-I0
        I1  => PinSignal_VGA_ADDR_HL_O1,                     -- ObjectKind=Pin|PrimaryId=SPRITEI-I1
        I2  => PinSignal_VGA_ADDR_HL_O2,                     -- ObjectKind=Pin|PrimaryId=SPRITEI-I2
        I3  => PinSignal_VGA_ADDR_HL_O3,                     -- ObjectKind=Pin|PrimaryId=SPRITEI-I3
        I4  => PinSignal_VGA_ADDR_HL_O9,                     -- ObjectKind=Pin|PrimaryId=SPRITEI-I4
        I5  => PinSignal_VGA_ADDR_HL_O10,                    -- ObjectKind=Pin|PrimaryId=SPRITEI-I5
        I6  => PinSignal_VGA_ADDR_HL_O11,                    -- ObjectKind=Pin|PrimaryId=SPRITEI-I6
        I7  => PinSignal_VGA_ADDR_HL_O12,                    -- ObjectKind=Pin|PrimaryId=SPRITEI-I7
        I8  => PinSignal_TILEMAPO_O0,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I8
        I9  => PinSignal_TILEMAPO_O1,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I9
        I10 => PinSignal_TILEMAPO_O2,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I10
        I11 => PinSignal_TILEMAPO_O3,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I11
        I12 => PinSignal_TILEMAPO_O4,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I12
        I13 => PinSignal_TILEMAPO_O5,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I13
        I14 => PinSignal_TILEMAPO_O6,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I14
        I15 => PinSignal_TILEMAPO_O7,                        -- ObjectKind=Pin|PrimaryId=SPRITEI-I15
        O   => PinSignal_SPRITEI_O                           -- ObjectKind=Pin|PrimaryId=SPRITEI-O[15..0]
      );

    -- Signal Assignments
    ---------------------
    NamedSignal_JoinA_TILE_SPRITE_JOIN_JB_C1 <= PinSignal_TILEMAPI_O; -- ObjectKind=Net|PrimaryId=NetTILEMAPI_O[15..0]
    NamedSignal_JoinB_TILE_SPRITE_JOIN_JB_C1 <= NamedSignal_JoinA_TILE_SPRITE_JOIN_JB_C1(10 downto 0);
    NamedSignal_VGA_ADDR                     <= VGA_ADDR; -- ObjectKind=Net|PrimaryId=VGA_ADDR[18..0]
    PinSignal_VGA_ADDR_HH_I                  <= NamedSignal_VGA_ADDR(18 downto 16); -- ObjectKind=Net|PrimaryId=VGA_ADDR[18..0]
    PowerSignal_GND                          <= '0'; -- ObjectKind=Net|PrimaryId=GND
    SPRITE_I                                 <= PinSignal_SPRITEI_O; -- ObjectKind=Net|PrimaryId=SPRITE_I[15..0]
    TILEMAP_I                                <= NamedSignal_JoinB_TILE_SPRITE_JOIN_JB_C1; -- ObjectKind=Net|PrimaryId=TILEMAP_I[10..0]
    VGA_DATA                                 <= SPRITE_O; -- ObjectKind=Net|PrimaryId=SPRITE_O[7..0]

end structure;
------------------------------------------------------------

