------------------------------------------------------------
-- VHDL TONE_SHEET
-- 2013 10 23 21 1 29
-- Created By "DXP VHDL Generator"
-- "Copyright (c) 2002-2004 Altium Limited"
------------------------------------------------------------

------------------------------------------------------------
-- VHDL TONE_SHEET
------------------------------------------------------------

Library IEEE;
Use     IEEE.std_logic_1164.all;

--synthesis translate_off
Library GENERIC_LIB;
Use     GENERIC_LIB.all;

--synthesis translate_on
Entity TONE_SHEET Is
  port
  (
    CLK     : In    STD_LOGIC;                               -- ObjectKind=Port|PrimaryId=CLK
    DATAIN  : In    STD_LOGIC_VECTOR(7 DOWNTO 0);            -- ObjectKind=Port|PrimaryId=DATAIN[7..0]
    SPEAKER : Out   STD_LOGIC;                               -- ObjectKind=Port|PrimaryId=SPEAKER
    TRIGGER : In    STD_LOGIC_VECTOR(7 DOWNTO 0);            -- ObjectKind=Port|PrimaryId=TRIGGER[7..0]
    USER    : In    STD_LOGIC                                -- ObjectKind=Port|PrimaryId=USER
  );
  attribute MacroCell : boolean;

End TONE_SHEET;
------------------------------------------------------------

------------------------------------------------------------
architecture structure of TONE_SHEET is
   Component CDIV5                                           -- ObjectKind=Part|PrimaryId=cdiv|SecondaryId=1
      port
      (
        CLKDV : out STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=cdiv-CLKDV
        CLKIN : in  STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=cdiv-CLKIN
      );
   End Component;

   Component CDIV10                                          -- ObjectKind=Part|PrimaryId=cdiv2|SecondaryId=1
      port
      (
        CLKDV : out STD_LOGIC;                               -- ObjectKind=Pin|PrimaryId=cdiv2-CLKDV
        CLKIN : in  STD_LOGIC                                -- ObjectKind=Pin|PrimaryId=cdiv2-CLKIN
      );
   End Component;

   Component clkdiv                                          -- ObjectKind=Sheet Symbol|PrimaryId=U_clkdiv
      port
      (
        clk     : in  STD_LOGIC;                             -- ObjectKind=Sheet Entry|PrimaryId=CLK_DIV_BLOCK.v-clk
        divisor : in  STD_LOGIC_VECTOR(31 downto 0);         -- ObjectKind=Sheet Entry|PrimaryId=CLK_DIV_BLOCK.v-divisor[31..0]
        oclk    : out STD_LOGIC                              -- ObjectKind=Sheet Entry|PrimaryId=CLK_DIV_BLOCK.v-oclk
      );
   End Component;

   Component tone_block                                      -- ObjectKind=Sheet Symbol|PrimaryId=U_tone_block
      port
      (
        clk      : in  STD_LOGIC;                            -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-clk
        dataOut  : out STD_LOGIC_VECTOR(31 downto 0);        -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-dataOut[31..0]
        tone_sel : in  STD_LOGIC_VECTOR(7 downto 0);         -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-tone_sel[7..0]
        trigger  : in  STD_LOGIC                             -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-trigger
      );
   End Component;


    Signal PinSignal_cdiv_CLKDV           : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netcdiv2_CLKIN
    Signal PinSignal_cdiv2_CLKDV          : STD_LOGIC; -- ObjectKind=Net|PrimaryId=Netcdiv2_CLKDV
    Signal PinSignal_U_clkdiv_oclk        : STD_LOGIC; -- ObjectKind=Net|PrimaryId=oclk
    Signal PinSignal_U_tone_block_dataOut : STD_LOGIC_VECTOR(31 downto 0); -- ObjectKind=Net|PrimaryId=divisor

   attribute VERILOGMODULE : string;
   attribute VERILOGMODULE of U_tone_block : Label is "tone_block";
   attribute VERILOGMODULE of U_clkdiv     : Label is "clkdiv";


begin
    U_tone_block : tone_block                                -- ObjectKind=Sheet Symbol|PrimaryId=U_tone_block
      Port Map
      (
        clk      => PinSignal_cdiv2_CLKDV,                   -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-clk
        dataOut  => PinSignal_U_tone_block_dataOut,          -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-dataOut[31..0]
        tone_sel => DATAIN,                                  -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-tone_sel[7..0]
        trigger  => USER                                     -- ObjectKind=Sheet Entry|PrimaryId=TONE_SEQUENCER.v-trigger
      );

    U_clkdiv : clkdiv                                        -- ObjectKind=Sheet Symbol|PrimaryId=U_clkdiv
      Port Map
      (
        clk     => CLK,                                      -- ObjectKind=Sheet Entry|PrimaryId=CLK_DIV_BLOCK.v-clk
        divisor => PinSignal_U_tone_block_dataOut,           -- ObjectKind=Sheet Entry|PrimaryId=CLK_DIV_BLOCK.v-divisor[31..0]
        oclk    => PinSignal_U_clkdiv_oclk                   -- ObjectKind=Sheet Entry|PrimaryId=CLK_DIV_BLOCK.v-oclk
      );

    cdiv2 : CDIV10                                           -- ObjectKind=Part|PrimaryId=cdiv2|SecondaryId=1
      Port Map
      (
        CLKDV => PinSignal_cdiv2_CLKDV,                      -- ObjectKind=Pin|PrimaryId=cdiv2-CLKDV
        CLKIN => PinSignal_cdiv_CLKDV                        -- ObjectKind=Pin|PrimaryId=cdiv2-CLKIN
      );

    cdiv : CDIV5                                             -- ObjectKind=Part|PrimaryId=cdiv|SecondaryId=1
      Port Map
      (
        CLKDV => PinSignal_cdiv_CLKDV,                       -- ObjectKind=Pin|PrimaryId=cdiv-CLKDV
        CLKIN => CLK                                         -- ObjectKind=Pin|PrimaryId=cdiv-CLKIN
      );

    -- Signal Assignments
    ---------------------
    SPEAKER <= PinSignal_U_clkdiv_oclk; -- ObjectKind=Net|PrimaryId=oclk

end structure;
------------------------------------------------------------

